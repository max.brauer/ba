## Forschungsdatenmanagement API

> Version: 1.0.0

This is the internal api used by the ui and the main webserver. See in the development notes for
more details.


### PUB `/ws/` Operation

The messages that are sent from the UI to the server

Accepts **one of** the following messages:

#### AuthSendCredentials

*Sends the login credentials*

Send the username and password of the user to the server


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"AuthSendCredentials"` |  |
| `.user` | R | `string` |  |
| `.password` | R | `string`, `<password>` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "AuthSendCredentials",
  "user": "abc",
  "password": "pa$$w0rd"
}
```

#### InfoRequest

*Request the current info from the server*

After the login the ui can request the current information from the server


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"InfoRequest"` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "InfoRequest"
}
```

#### DataInterviewAddRequest

*Add Interview Request*

Creates a new interview entry at the server and fill with empty data. The server
will respond with the created interview entry


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"DataInterviewAddRequest"` |  |
| `.database` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "DataInterviewAddRequest",
  "database": "db-main"
}
```

#### DataInterviewListPersonRequest

*Request the list of persons in an interview*

Request the full list of all persons that are assigned to an interview.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"DataInterviewListPersonRequest"` |  |
| `.id` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "DataInterviewListPersonRequest",
  "id": "#id-0123456789abcdef"
}
```

#### DataPersonAddRequest

*Add Person Request*

Creates a new person entry at the server and fill with empty data.
The server will respond with the created interview entry.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"DataPersonAddRequest"` |  |
| `.database` | R | `string` |  |
| `.interview` | R | `string` |  |
| `.role` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "DataPersonAddRequest",
  "database": "db-main",
  "interview": "abc",
  "role": "abc"
}
```

#### DataInterviewGet

*Request the data of a single interview*

Get the full data of a single interview


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"DataInterviewGet"` |  |
| `.id` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "DataInterviewGet",
  "id": "#id-0123456789abcdef"
}
```

#### DataPersonGet

*Request the data of a single person*

Get the full data of a single person with history of attributes


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"DataPersonGet"` |  |
| `.id` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "DataPersonGet",
  "id": "#id-0123456789abcdef"
}
```

#### FilePersonFetch

*Fetch a list of files from an person*

This will send the list of all files attached to an person.
If the `root` field is not set it will send all files attached to the
root (they have no parent files). Otherwise it will send the direct
decendends of this root


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"FilePersonFetch"` |  |
| `.person` | R | `string` |  |
| `.root` | N | `null`, `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "FilePersonFetch",
  "person": "abc"
}
```

#### FileFetch

*Fetch a single file*

This will fetch a single file by its id


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"FileFetch"` |  |
| `.id` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "FileFetch",
  "id": "#id-0123456789abcdef"
}
```

#### EditInterviewSend

*Send the edit of the interview*

This sends the the edit of an interview. The editable fields are optional. They are only
updated if they are set.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"EditInterviewSend"` |  |
| `.id` | R | `string` |  |
| `.delete` |  | `bool` | If set to true this will delete this interview |
| `.time` | N | `null`, `string`, `<date-time>` |  |
| `.location` | N | `null`, `string` |  |
| `.interviewer` | N | `null`, `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "EditInterviewSend",
  "id": "#id-0123456789abcdef"
}
```

#### EditPersonSend

*Send the edit of the person*

This sends the the edit of an person. The editable fields are optional. They are only
updated if they are set.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"EditPersonSend"` |  |
| `.id` | R | `string` |  |
| `.delete` |  | `bool` | If set to true this will delete this person |
| `.name` | N | `null`, `string` |  |
| `.contact` | N | `null`, `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "EditPersonSend",
  "id": "#id-0123456789abcdef"
}
```

#### EditAttributeSend

*Send the edit of an attribute of an person.*

This sends the the edit of an attribute. If the attribute doesn't exists before it will be
creates as long the schema of the user allows it.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"EditAttributeSend"` |  |
| `.id` | R | `string` |  |
| `.key` | R | `string` |  |
| `.value` | R | `string`, `number`, `bool` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "EditAttributeSend",
  "id": "#id-0123456789abcdef",
  "key": "abc",
  "value": "abc"
}
```
```json
{
  "$type": "EditAttributeSend",
  "id": "#id-0123456789abcdef",
  "key": "abc",
  "value": 3.14
}
```
```json
{
  "$type": "EditAttributeSend",
  "id": "#id-0123456789abcdef",
  "key": "abc",
  "value": false
}
```

#### SearchSend

*Send a new search request to the server*

The search query is a complex string that allows detailed sorting and filtering


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"SearchSend"` |  |
| `.query` | R | `string` |  |
| `.uiId` |  | `string` | This is a field that can be set by the UI to identify the messages later. |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "SearchSend",
  "query": "abc"
}
```

#### SearchResponse

*Cancel a search request*

If the user changes its query or leaves the page the search should be canceld. This is not
necessary if the results are already finished.

The server will send a last `SearchResponse` message to notify the cancellation


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"SearchResponse"` |  |
| `.query` | R | `string` |  |
| `.uiId` |  | `string` | This is a field that can be set by the UI to identify the messages later. |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "SearchResponse",
  "query": "abc"
}
```

#### SchemaRequest

*Requests the data of a schema*

The schema needs to be registered at the server before. It is recommended to cache the
result because it is unlikely that it will change.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"SchemaRequest"` |  |
| `.name` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "SchemaRequest",
  "name": "abc"
}
```

### SUB `/ws/` Operation

The messages that are sent from the server to the UI

Accepts **one of** the following messages:

#### AuthInvalidCredentials

*The given credentials was invalid*

The database cannot be opened with the provided credentials


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"AuthInvalidCredentials"` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "AuthInvalidCredentials"
}
```

#### AuthRequest2FA

*Show the 2FA screen*

The first authentication step pass. Now the user has to fullfil the 2FA step.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"AuthRequest2FA"` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "AuthRequest2FA"
}
```

#### Auth2FASucceed

*The 2FA step succeeded*

The UI can now return to its previous page (or home screen)


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"Auth2FASucceed"` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "Auth2FASucceed"
}
```

#### InfoSend

*Send the current server info from the server to the ui*

This is send after the `info-request` message


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"InfoSend"` |  |
| `.user` | R | `string` |  |
| `.database` | R | `array` |  |
| `.database[]` | R | `object` |  |
| `.database[].name` | R | `string` |  |
| `.database[].connected` | R | `bool` |  |
| `.database[].hasConfidential` | R | `bool` | This flag is only true if the database is connected and the confidential data is available |
| `.database[].roleSchema` | R | `string` |  |
| `.schemas` | R | `array` |  |
| `.schemas[]` | R | `string` |  |
| `.uploadToken` | R | `string` | This upload token is used to verify the author of an upload to the REST Api. This token can be used for the following things: - upload new file - update file - download file |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "user": "alice",
  "database": [
    {
      "name": "main",
      "connected": true
    },
    {
      "name": "bob",
      "connected": false
    }
  ],
  "uploadToken": "\u003Cinsert your token here\u003E"
}
```

#### DataInterviewSend

*Sends the data of an interview*

This message is a response of `data-interview-get` and will provide the
full interview. If no interview was found the `data` field will be `null`.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"DataInterviewSend"` |  |
| `.id` | R | `string` |  |
| `.data` | RN | `null`, `object` |  |
| `.data.id` | R | `string` |  |
| `.data.database` | R | `string` |  |
| `.data.created` | R | `string`, `<date-time>` |  |
| `.data.modified` | R | `string`, `<date-time>` |  |
| `.data.deleted` | R | `bool` |  |
| `.data.time` | R | `string`, `<date-time>` |  |
| `.data.location` | R | `string` |  |
| `.data.interviewer` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "DataInterviewSend",
  "id": "#id-0123456789abcdef",
  "data": null
}
```
```json
{
  "$type": "DataInterviewSend",
  "id": "#id-0123456789abcdef",
  "data": {
    "id": "#id-0123456789abcdef",
    "database": "db-main",
    "created": "2001-01-01T15:17:19.001",
    "modified": "2001-01-01T15:17:19.001",
    "deleted": false,
    "time": "2001-01-01T15:17:19.001",
    "location": "abc",
    "interviewer": "abc"
  }
}
```

#### DataInterviewListPersonSend

*Send the list of persons in an interview*

This is the response to the `DataInterviewListPersonRequest` message. This will only
contain a partial list. The ui has to merge the results.

There is no last message field. It is possible that responses are sent twice if two requests
are sent at the same time.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"DataInterviewListPersonSend"` |  |
| `.id` | R | `string` |  |
| `.persons` | R | `array` |  |
| `.persons[]` | R | `object` |  |
| `.persons[].id` | R | `string` |  |
| `.persons[].role` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.

> Examples:

```json
{
  "$type": "DataInterviewListPersonSend",
  "id": "#id-0123456789abcdef",
  "persons": [
    {
      "id": "#id-0123456789abcdef",
      "role": "abc"
    }
  ]
}
```

#### DataPersonSend

*Sends the data of an person*

This message is a response of `data-person-get` and will provide the
full person. If no person was found the `data` field will be `null`.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"DataPersonSend"` |  |
| `.id` | R | `string` |  |
| `.data` | RN | `null`, `object` |  |
| `.data.id` | R | `string` |  |
| `.data.database` | R | `string` |  |
| `.data.created` | R | `string`, `<date-time>` |  |
| `.data.modified` | R | `string`, `<date-time>` |  |
| `.data.deleted` | R | `bool` |  |
| `.data.interview` | N | `null`, `string` |  |
| `.data.name` | N | `string`, `null` |  |
| `.data.altName` | R | `string` |  |
| `.data.contact` | N | `string`, `null` |  |
| `.data.role` | R | `string` |  |
| `.data.attributes` | R | `array` |  |
| `.data.attributes[]` | R | `object` |  |
| `.data.attributes[].key` | R | `string` |  |
| `.data.attributes[].created` | R | `string`, `<date-time>` |  |
| `.data.attributes[].modified` | R | `string`, `<date-time>` |  |
| `.data.attributes[].entries` | R | `array` | the complete history of this attribute |
| `.data.attributes[].entries[]` | R | `object` |  |
| `.data.attributes[].entries[].date` | R | `string`, `<date-time>` |  |
| `.data.attributes[].entries[].value` | R | `string`, `int`, `number`, `bool` |  |
| `.data.attributes[].entry` | R | `object` |  |
| `.data.attributes[].entry.date` | R | `string`, `<date-time>` |  |
| `.data.attributes[].entry.value` | R | `string`, `int`, `number`, `bool` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "DataPersonSend",
  "id": "#id-0123456789abcdef",
  "data": null
}
```
```json
{
  "$type": "DataPersonSend",
  "id": "#id-0123456789abcdef",
  "data": {
    "id": "#id-0123456789abcdef",
    "database": "db-main",
    "created": "2001-01-01T15:17:19.001",
    "modified": "2001-01-01T15:17:19.001",
    "deleted": false,
    "altName": "abc",
    "role": "abc",
    "attributes": [
      {
        "key": "abc",
        "created": "2001-01-01T15:17:19.001",
        "modified": "2001-01-01T15:17:19.001",
        "entries": [
          {
            "date": "2001-01-01T15:17:19.001",
            "value": "abc"
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": 42
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": 3.14
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": false
          }
        ],
        "entry": {
          "date": "2001-01-01T15:17:19.001",
          "value": "abc"
        }
      },
      {
        "key": "abc",
        "created": "2001-01-01T15:17:19.001",
        "modified": "2001-01-01T15:17:19.001",
        "entries": [
          {
            "date": "2001-01-01T15:17:19.001",
            "value": "abc"
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": 42
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": 3.14
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": false
          }
        ],
        "entry": {
          "date": "2001-01-01T15:17:19.001",
          "value": 42
        }
      },
      {
        "key": "abc",
        "created": "2001-01-01T15:17:19.001",
        "modified": "2001-01-01T15:17:19.001",
        "entries": [
          {
            "date": "2001-01-01T15:17:19.001",
            "value": "abc"
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": 42
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": 3.14
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": false
          }
        ],
        "entry": {
          "date": "2001-01-01T15:17:19.001",
          "value": 3.14
        }
      },
      {
        "key": "abc",
        "created": "2001-01-01T15:17:19.001",
        "modified": "2001-01-01T15:17:19.001",
        "entries": [
          {
            "date": "2001-01-01T15:17:19.001",
            "value": "abc"
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": 42
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": 3.14
          },
          {
            "date": "2001-01-01T15:17:19.001",
            "value": false
          }
        ],
        "entry": {
          "date": "2001-01-01T15:17:19.001",
          "value": false
        }
      }
    ]
  }
}
```

#### FilePersonSend

*Send the requested list of file of an person*

Send the list of files that the user has with `file-person-fetch` requested.
If no files are found, the root or the person doesn't exists the list
will be empty.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"FilePersonSend"` |  |
| `.person` | R | `string` |  |
| `.root` | RN | `null`, `string` |  |
| `.data` | R | `array` |  |
| `.data[]` | R | `object` |  |
| `.data[].id` | R | `string` |  |
| `.data[].database` | R | `string` |  |
| `.data[].created` | R | `string`, `<date-time>` |  |
| `.data[].modified` | R | `string`, `<date-time>` |  |
| `.data[].deleted` | R | `bool` |  |
| `.data[].person` | R | `string` |  |
| `.data[].parent` | N | `null`, `string` |  |
| `.data[].name` | R | `string` |  |
| `.data[].mime` | R | `string` | The MIME type of this file. If this file is a folder the string `folder` is used instead. |
| `.data[].confidential` | R | `bool` |  |
| `.data[].entries` | R | `array` | the complete history of this file. This is empty if its a folder |
| `.data[].entries[]` | R | `object` |  |
| `.data[].entries[].date` | R | `string`, `<date-time>` |  |
| `.data[].entries[].exists` | R | `bool` | This flag describes if this file version is on the local disk available and ready for download. |
| `.data[].entry` | N | `null`, `object` | the most reason file entry. This is empty for folders |
| `.data[].entry.date` | R | `string`, `<date-time>` |  |
| `.data[].entry.exists` | R | `bool` | This flag describes if this file version is on the local disk available and ready for download. |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "FilePersonSend",
  "person": "abc",
  "root": null,
  "data": [
    {
      "id": "#id-0123456789abcdef",
      "database": "db-main",
      "created": "2001-01-01T15:17:19.001",
      "modified": "2001-01-01T15:17:19.001",
      "deleted": false,
      "person": "abc",
      "name": "abc",
      "mime": "text/plain",
      "confidential": false,
      "entries": [
        {
          "date": "2001-01-01T15:17:19.001",
          "exists": false
        }
      ]
    }
  ]
}
```
```json
{
  "$type": "FilePersonSend",
  "person": "abc",
  "root": "abc",
  "data": [
    {
      "id": "#id-0123456789abcdef",
      "database": "db-main",
      "created": "2001-01-01T15:17:19.001",
      "modified": "2001-01-01T15:17:19.001",
      "deleted": false,
      "person": "abc",
      "name": "abc",
      "mime": "text/plain",
      "confidential": false,
      "entries": [
        {
          "date": "2001-01-01T15:17:19.001",
          "exists": false
        }
      ]
    }
  ]
}
```

#### FileSend

*Send the requested info of a single file*

Sends the info of a single file if it was requested by an `file-fetch`.
If this entry doesn't exists the `data` field will be `null`.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"FileSend"` |  |
| `.id` | R | `string` |  |
| `.data` | RN | `null`, `object` |  |
| `.data.id` | R | `string` |  |
| `.data.database` | R | `string` |  |
| `.data.created` | R | `string`, `<date-time>` |  |
| `.data.modified` | R | `string`, `<date-time>` |  |
| `.data.deleted` | R | `bool` |  |
| `.data.person` | R | `string` |  |
| `.data.parent` | N | `null`, `string` |  |
| `.data.name` | R | `string` |  |
| `.data.mime` | R | `string` | The MIME type of this file. If this file is a folder the string `folder` is used instead. |
| `.data.confidential` | R | `bool` |  |
| `.data.entries` | R | `array` | the complete history of this file. This is empty if its a folder |
| `.data.entries[]` | R | `object` |  |
| `.data.entries[].date` | R | `string`, `<date-time>` |  |
| `.data.entries[].exists` | R | `bool` | This flag describes if this file version is on the local disk available and ready for download. |
| `.data.entry` | N | `null`, `object` | the most reason file entry. This is empty for folders |
| `.data.entry.date` | R | `string`, `<date-time>` |  |
| `.data.entry.exists` | R | `bool` | This flag describes if this file version is on the local disk available and ready for download. |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "FileSend",
  "id": "#id-0123456789abcdef",
  "data": null
}
```
```json
{
  "$type": "FileSend",
  "id": "#id-0123456789abcdef",
  "data": {
    "id": "#id-0123456789abcdef",
    "database": "db-main",
    "created": "2001-01-01T15:17:19.001",
    "modified": "2001-01-01T15:17:19.001",
    "deleted": false,
    "person": "abc",
    "name": "abc",
    "mime": "text/plain",
    "confidential": false,
    "entries": [
      {
        "date": "2001-01-01T15:17:19.001",
        "exists": false
      }
    ]
  }
}
```

#### FileUpdated

*File was updated on the server*

If the user has updated a file with the REST Api the server will send this
message after successfull upload.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"FileUpdated"` |  |
| `.id` | R | `string` |  |
| `.database` | R | `string` |  |
| `.created` | R | `string`, `<date-time>` |  |
| `.modified` | R | `string`, `<date-time>` |  |
| `.deleted` | R | `bool` |  |
| `.person` | R | `string` |  |
| `.parent` | N | `null`, `string` |  |
| `.name` | R | `string` |  |
| `.mime` | R | `string` | The MIME type of this file. If this file is a folder the string `folder` is used instead. |
| `.confidential` | R | `bool` |  |
| `.entries` | R | `array` | the complete history of this file. This is empty if its a folder |
| `.entries[]` | R | `object` |  |
| `.entries[].date` | R | `string`, `<date-time>` |  |
| `.entries[].exists` | R | `bool` | This flag describes if this file version is on the local disk available and ready for download. |
| `.entry` | N | `null`, `object` | the most reason file entry. This is empty for folders |
| `.entry.date` | R | `string`, `<date-time>` |  |
| `.entry.exists` | R | `bool` | This flag describes if this file version is on the local disk available and ready for download. |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "FileUpdated",
  "id": "#id-0123456789abcdef",
  "database": "db-main",
  "created": "2001-01-01T15:17:19.001",
  "modified": "2001-01-01T15:17:19.001",
  "deleted": false,
  "person": "abc",
  "name": "abc",
  "mime": "text/plain",
  "confidential": false,
  "entries": [
    {
      "date": "2001-01-01T15:17:19.001",
      "exists": false
    }
  ]
}
```

#### EditAccept

*The successful answer of the server of an edit request*

After the client has sent an edit request this will be the answer if the edit was successful


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"EditAccept"` |  |
| `.id` | R | `string` |  |
| `.key` | N | `null`, `string` | Is `null` if deleted |
| `.target` | R | `string`, `"Interview"`, `"Person"`, `"Attribute"` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "EditAccept",
  "id": "#id-0123456789abcdef",
  "target": "Interview"
}
```
```json
{
  "$type": "EditAccept",
  "id": "#id-0123456789abcdef",
  "target": "Person"
}
```
```json
{
  "$type": "EditAccept",
  "id": "#id-0123456789abcdef",
  "target": "Attribute"
}
```

#### EditDenied

*The edit cannot be done after an edit request*

After the client has sent an edit request which cant be fullfilled this will be the answer.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"EditDenied"` |  |
| `.id` | R | `string` |  |
| `.key` | N | `null`, `string` | Is `null` if deleted |
| `.target` | R | `string`, `"Interview"`, `"Person"`, `"Attribute"` |  |
| `.reasonCode` | R | `"IdNotFound"`, `"SchemaNotFound"`, `"AttributeNotAvailable"`, `"AttributeForbidden"`, `"InvalidValueType"`, `"Generic"` |  |
| `.reason` | R | `string` | A simple description of this error in english. |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "EditDenied",
  "id": "#id-0123456789abcdef",
  "target": "Interview",
  "reasonCode": "IdNotFound",
  "reason": "abc"
}
```
```json
{
  "$type": "EditDenied",
  "id": "#id-0123456789abcdef",
  "target": "Person",
  "reasonCode": "IdNotFound",
  "reason": "abc"
}
```
```json
{
  "$type": "EditDenied",
  "id": "#id-0123456789abcdef",
  "target": "Attribute",
  "reasonCode": "IdNotFound",
  "reason": "abc"
}
```

#### SearchResponse

*Respond with the search result*

Response with a partial result. The server will continuesly send more of these frames if
it has more data.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"SearchResponse"` |  |
| `.query` | R | `string` |  |
| `.uiId` |  | `string` | This is an exact copy of the `uiId` field of `SearchSend` and only set if it was set in the request. |
| `.isLast` | R | `bool` | If this was the last message to this search query this field will be set `true`. |
| `.order` | R | `int` | The internal message order of the server. |
| `.result` | R | `array` |  |
| `.result[]` | R | `object` |  |
| `.result[].id` | R | `string` |  |
| `.result[].database` | R | `string` |  |
| `.result[].type` | R | `"Interview"`, `"Person"`, `"File"` |  |
| `.result[].role` |  | `string` | Only used if `type` is `Person` |
| `.result[].title` | R | `string` |  |
| `.result[].info` | R | `array` |  |
| `.result[].info[]` | R | `object` |  |
| `.result[].info[].name` | R | `string` |  |
| `.result[].info[].value` | R | `string` |  |
| `.result[].found` |  | `array` |  |
| `.result[].found[]` | R | `object` |  |
| `.result[].found[].field` | R | `string` |  |
| `.result[].found[].type` | R | `"Field"`, `"Attribute"` |  |
| `.result[].found[].value` | R | `array` |  |
| `.result[].found[].value[]` | R |  |  |
| `.result[].found[].value[].<0>` | C | `object` |  |
| `.result[].found[].value[].<0>.type` | R | `"Ellipsis"` |  |
| `.result[].found[].value[].<1>` | C | `object` |  |
| `.result[].found[].value[].<1>.type` | R | `"Context"`, `"Term"` |  |
| `.result[].found[].value[].<1>.value` | R | `string` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *C*: This is one of multiple choices. You have to select one of the choices.

> Examples:

```json
{
  "$type": "SearchResponse",
  "query": "abc",
  "isLast": false,
  "order": 42,
  "result": [
    {
      "id": "#id-0123456789abcdef",
      "database": "db-main",
      "type": "Interview",
      "title": "abc",
      "info": [
        {
          "name": "abc",
          "value": "abc"
        }
      ]
    },
    {
      "id": "#id-0123456789abcdef",
      "database": "db-main",
      "type": "Person",
      "title": "abc",
      "info": [
        {
          "name": "abc",
          "value": "abc"
        }
      ]
    },
    {
      "id": "#id-0123456789abcdef",
      "database": "db-main",
      "type": "File",
      "title": "abc",
      "info": [
        {
          "name": "abc",
          "value": "abc"
        }
      ]
    }
  ]
}
```

#### SchemaResponse

*Sends the data of a schema*

This contains the data of the requested schema. If the name was invalid an empty
schema will be returned.


| Path | Flags | Type | Description |
|-|-|-|-|
| `.$type` | R | `"SchemaResponse"` |  |
| `.name` | R | `string` |  |
| `.schema` | R | `array` |  |
| `.schema[]` | R | `object` |  |
| `.schema[].name` | R | `string` |  |
| `.schema[].abstract` | R | `bool` |  |
| `.schema[].parent` | RN | `null`, `string` |  |
| `.schema[].attributes` | R | `object` |  |

> **Flags:**
> - *R*: This field is required. This path must always exists.
> - *N*: The value of this field can be `null`.

> Examples:

```json
{
  "$type": "SchemaResponse",
  "name": "abc",
  "schema": []
}
```

