namespace AsyncApiRenderer.Data;

public class Components
{
    public Dictionary<string, Message> Messages { get; set; }
        = new Dictionary<string, Message>();

    public Dictionary<string, JsonSchemaNode> Schemas { get; set; }
        = new Dictionary<string, JsonSchemaNode>();

    public void Deref()
    {
        var schemas = new Dictionary<string, JsonSchemaNode>();
        foreach (var (key, value) in Schemas)
            schemas[key] = value.Deref(Schemas);
        Schemas = schemas;

        foreach (var (_, message) in Messages)
            message.Payload = message.Payload.Deref(Schemas);
        
        Schemas.Clear();
    }

    public void Collapse()
    {
        foreach (var (_, message) in Messages)
            message.Payload = message.Payload.Collapse();
    }
}
