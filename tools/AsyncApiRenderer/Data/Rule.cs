namespace AsyncApiRenderer.Data;

public class Rule
{
    public string Path { get; set; } = "";

    public bool IsRequired { get; set; }

    public bool IsNullable { get; set; }

    public bool IsChoice { get; set; }

    public bool isAdditional { get; set; }

    public HashSet<JsonSchemaType>? Types { get; set; }

    public string? Format { get; set; }

    public List<string>? Enum { get; set; }

    public string? Description { get; set; }
}