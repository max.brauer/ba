module Data.Info exposing (..)

import Data exposing (..)
import Json.Decode as JD exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Time exposing (Posix)
import Iso8601

type alias Info =
    { user: String
    , database: List DbInfo
    , schemas: List String
    , uploadToken: String
    }

decodeInfo : Decoder Info
decodeInfo =
    JD.succeed Info
        |> required "user" JD.string
        |> required "database" (JD.list decodeDbInfo)
        |> required "schemas" (JD.list JD.string)
        |> required "uploadToken" JD.string

type alias DbInfo =
    { name: String
    , connected: Bool
    , hasConfidential: Bool
    , roleSchema: String
    , access: List DbAccess
    , unlocked: Maybe Posix
    }

decodeDbInfo : Decoder DbInfo
decodeDbInfo =
    JD.succeed DbInfo
        |> required "name" JD.string
        |> required "connected" JD.bool
        |> required "hasConfidential" JD.bool
        |> required "roleSchema" JD.string
        |> required "access" (JD.list decodeDbAccess)
        |> required "unlocked" (JD.nullable Iso8601.decoder)

type alias DbAccess =
    { user: String
    , userId: String
    , fidoDescriptor: Bool -- we can have more details later but right now it is important to know
    , created: Posix
    }

decodeDbAccess : Decoder DbAccess
decodeDbAccess =
    JD.succeed DbAccess
        |> required "user" JD.string
        |> required "user-id" JD.string
        |> required "fido" JD.bool
        |> required "created" Iso8601.decoder
