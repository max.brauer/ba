module Panes.Navigation exposing (..)

import Panes exposing (..)
import Html exposing (Html, div, text)
import Html.Attributes as HA exposing (class)
import Html.Events as HE
import Triple exposing (triple)
import PaneMeta
import History exposing (History)
import Network.Messages
import Dict
import Config

type alias Model = ()

type Msg
    = None
    | SendEvent Event

type alias Button =
    { title: String
    , action: Msg
    }

init : Init () Model Msg
init _ () = triple () Cmd.none []

view : View Model Msg
view data =
    div [ class "pane-nav" ]
        [ viewButtons
        , viewHistory data.meta.history
        , div [ class "pane-nav-version" ]
            [ text <| "Version: " ++ Config.version ++ " - " ++ Config.buildVersion ]
        ]

viewButtons : Html Msg
viewButtons =
    div [ class "pane-nav-buttons" ]
    <| List.map
        (\ {title, action } ->
            div
                [ class "pane-nav-button"
                , HE.onClick action
                ]
                [ text title ]
        )
        [ Button "Home" <| SendEvent <| Open <| History.Home
        , Button "Search" <| SendEvent <| Open 
            <| History.Search Nothing ""
        , Button "All Interviews" <| SendEvent <| Open 
            <| History.Search (Just "All Interviews") "type:interview sort:none"
        , Button "All Persons" <| SendEvent <| Open
            <| History.Search (Just "All Persons") "type:person sort:none"
        -- , Button "Debug: Open Interview 0" <| SendEvent <| Open <| History.Interview "0"
        -- , Button "Debug: Open Person 0" <| SendEvent <| Open <| History.Person "0"
        ]
    
viewHistory : List History -> Html Msg
viewHistory history =
    div [ class "pane-nav-history" ]
        [ div [ class "pane-nav-history-title" ]
            [ text "Navigation History:" ]
        , div [ class "pane-nav-history-entries" ]
            <| List.map
                (\entry ->
                    (\name ->
                        div
                            [ class "pane-nav-history-entry"
                            , HA.title name
                            , HE.onClick <| SendEvent <| Panes.Open entry
                            ]
                            [ text name ]
                    )
                    <| case entry of
                        History.Home ->
                            "Home"
                        History.Interview id ->
                            "Interview: " ++ id
                        History.Person id ->
                            "Person: " ++ id
                        History.Search Nothing query ->
                            "Search: " ++ String.left 40 query
                        History.Search (Just alt) _ ->
                            alt
                )
                history
        ]

update : Update Model Msg
update msg data =
    case msg of
        None -> triple data.state Cmd.none []
        SendEvent event -> triple data.state Cmd.none [ event ]

apply : Apply Model Msg
apply event data =
    case event of
        Network.Messages.InfoSend info ->
            triple data.state Cmd.none
            <| List.map (Send << Network.Messages.SchemaRequest)
            <| List.filter
                (\x -> not <| Dict.member x data.meta.schemas)
            <| info.schemas
        Network.Messages.DataPersonAddSend id ->
            triple
                data.state
                Cmd.none
                [ Panes.Open
                    <| History.Person id
                ]
        Network.Messages.DataInterviewAddSend id ->
            triple
                data.state
                Cmd.none
                [ Panes.Open
                    <| History.Interview id
                ]
        Network.Messages.SendNotification notification ->
            triple data.state Cmd.none
                [ Panes.SetNotification
                    { notification
                    | id = Maybe.map (\id -> "server-" ++ id)
                        notification.id
                    }
                ]
        _ -> triple data.state Cmd.none []

save : Save Model
save _ = []

subscription : Subscription Model Msg
subscription data = Sub.none
