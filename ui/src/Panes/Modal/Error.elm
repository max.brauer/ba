module Panes.Modal.Error exposing (..)

import Html exposing (div, text)
import Html.Attributes exposing (class)
import Triple exposing (triple)
import Panes exposing (..)
import Http

component : RawComponent () Model Never
component =
    { init = \_ _ -> (Model "" [], Cmd.none, [])
    , view = view
    , title = title
    , update = update
    , apply = apply
    , save = save
    , subscription = subscription
    }

type alias Model =
    { title: String
    , parts: List ErrorPart
    }

type ErrorPart
    = Description String
    | Internal String

initParsingError : Init String Model Never
initParsingError _ error =
    triple
        { title = "Parsing Error"
        , parts =
            [ Description
                <| "A message from the internal server couldn't be parsed. Please send this error "
                ++ "including the following data to your support. This will help to fix it in a "
                ++ "future release."
            , Internal error
            ]
        }
        Cmd.none
        []

initHttpError : Init Http.Error Model Never
initHttpError _ error =
    triple
        { title = "Network Error"
        , parts =
            case error of
                Http.BadUrl url ->
                    [ Description
                        <| "The provided URL is invalid and return no successful result. "
                        ++ "This should normally not happen. Is your local application running? "
                        ++ "Try to contact your support for more help about this."
                    , Internal url
                    ]
                Http.Timeout ->
                    [ Description
                        <| "It seems like that your local application hangs. Try to wait a little "
                        ++ "do it again. If this won't help try to restart the application or the "
                        ++ "computer. Contact your support for more help about this."
                    ]
                Http.NetworkError ->
                    [ Description
                        <| "Your network seems to interrupted. This should never happen because "
                        ++ "this application doesn't rely on the external network. Your setup is "
                        ++ "broken. Try to contact your support!"
                    ]
                Http.BadStatus status ->
                    [ Description
                        <| "You got an unexpected status from the background server. You cannot do "
                        ++ "something about this. Try to contact your support!"
                    , Internal <| "Status: " ++ String.fromInt status
                    ]
                Http.BadBody body ->
                    [ Description
                        <| "The background server gave you an expected answer. You cannot do "
                        ++ "something about this. Try to contact your support!"
                    , Internal body
                    ]
        }
        Cmd.none
        []

view : View Model Never
view { state } =
    div [ class "modal-error" ]
    <| List.map
        (\part ->
            case part of
                Description value ->
                    div [ class "modal-error-description" ]
                        [ text value ]
                Internal value ->
                    div [ class "modal-error-internal" ]
                        [ text value ]
        )
    <| state.parts

title : Title Model
title { state } = state.title

update : Update Model Never
update _ { state } = triple state Cmd.none []

apply : Apply Model Never
apply _ { state } = triple state Cmd.none []

save : Save Model
save _ = []

subscription : Subscription Model Never
subscription _ = Sub.none
