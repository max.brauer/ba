#!/bin/bash

function download () {

	next="https://api.svgrepo.com/collection/?term=interaction-set&limit=10&start=0"

	while [[ ! "$next" = "" ]]; do
		data="$(curl "$next" 2> /dev/null)"
		next="$(echo "$data" | jq -r ".next")"
		echo "$data" | jq -r '.icons[] | "wget -nv -O \"" + .slug + "-" + .id + ".svg\" \"https://www.svgrepo.com/show/" + .id + "/" + .slug + ".svg\""'
	done | bash

}

function find_dupplicates () {
	jdupes -f . | grep -vE "^$" | xargs -I{} rm {}
}

download
find_dupplicates
