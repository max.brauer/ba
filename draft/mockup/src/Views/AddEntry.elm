module Views.AddEntry exposing (..)

import Html exposing (Html,div,text)
import Html.Attributes as HA exposing (class)

lorem : String
lorem = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."

viewSingleEntry : String -> Html msg
viewSingleEntry title =
    viewCustomEntry title
    <| Html.input
        [ HA.type_ "text" 
        ] []

viewFilledEntry : String -> Bool -> String -> Html msg
viewFilledEntry title long content =
    viewCustomEntry title
    <| if long
        then Html.textarea
            [ HA.value content ]
            []
        else  Html.input
            [ HA.type_ "text"
            , HA.value content
            ] []
viewCustomEntry : String -> Html msg -> Html msg
viewCustomEntry title content =
    div [ class "entry" ]
        [ div [ class "title" ]
            [ text title ]
        , div [ class "input" ]
            [ content ]
        ]
    
viewUserList : List String -> Html msg
viewUserList users =
    div [ class "user-bar" ]
        <|  (\x -> x ++
                [ div [ class "button", class "add" ]
                    [ text "+" ]
                ]
            )
        <| List.map
            (\name ->
                div [ class "button", class "user" ]
                    [ text name ]
            )
        <| users

viewUser : Html msg
viewUser =
    div [ class "entry-group" ]
        [ viewCustomEntry "type" <| text "Doctor"
        , viewFilledEntry "name" False "Max Mustermann"
        -- Note: I just searched for hospitals in London and picked the first one
        , viewFilledEntry "contact" True "The Royal London Hospital For Integrated Medicine\n60 Great Ormond Street, London, WC1N 3HR"
        , viewFilledEntry "location" False "London"
        , viewFilledEntry "occupation" False "psychiatrist"
        , viewFilledEntry "personal experience" True "has to work with patients for 24 years"
        , viewFilledEntry "expert view" True lorem
        ]

view : model -> Html msg
view _ =
    div [ class "view-add-entry" ]
        [ Html.h2 [] [ text "Interview" ]
        , div [ class "entry-group" ]
            [ viewFilledEntry "location" False "The Royal London Hospital For Integrated Medicine"
            , viewFilledEntry "time" False "2024-05-17 17:54"
            , viewFilledEntry "interviewer" False "Ralf Muster"
            ]
        , Html.h2 [] [ text "Interviewee" ]
        , viewUserList [ "Max Mustermann", "Angelina Brown", "Rudolf Summer" ]
        , viewUser
        ]
