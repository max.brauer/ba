# Wahl der Programmiersprachen

- Es sind Programmiersprachen, in denen ich sehr vertraut bin und den Umgang sehr gut beherrsche. Es
  sind keine extra Einarbeitszeiten nötig.
- Es besteht ein striktes Zeitlimit. Es können jederzeit neue Features verlangt werden oder das
  Modell umgebaut werden müssen. In den Programmiersprachen lassen sich Änderungen durchführen, ohne
  neue aufwändige Tests durchführen zu müssen, da der Compiler das meiste schon für einen abnimmt.
