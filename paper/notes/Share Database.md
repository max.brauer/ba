# Datenbank teilen

Anleitung der notwendigen Schritte für das Teilen der lokalen Datenbank von Alice, damit Bob sie
einsehen kann.

> Hier sind die technischen Details inbegriffen.

1. Die Software von Bob erstellt einen Public Key und Privat Key. Diese werden nur bei ihn lokal
    hinterlegt.
2. Bob bekommt seinen Public Key angezeigt. Er schickt diesen an Alice. Der Public Key muss nicht
    verschlüsselt werden oder andere Sicherheitsmaßnahmen erfahren.
3. Alice importiert den Public Key von Bob in ihre Software.
4. Alice exportiert ihre Zustand und verschlüsselt diesen mit Bobs Public Key.
    - Dafür muss Alice sich erneut authentifizieren.
5. Alice verschickt den verschlüsselten Datensatz an Bob über eine unsichere Leitung.
6. Bob erhält den Datensatz und import diesen in seine Software. Die Software entschlüsselt
    automatisch mit seinen privaten Schlüssel.
7. Der entschlüsselte Datensatz wird bei Bob in die Datenbank integriert. Er muss sich erneut
    authentifizieren und ab sofort kann er den Datensatz lokal nutzen.

Vorteile:
- Sobald der Public Key geteilt wurde kann Alice immer wieder neue Daten an Bob schicken.
- Niemand außer Bob kann die Daten lesen. Es existiert somit gleichzeitig eine Zugriffskontrolle.
- Der Private Key wird nur beim Importieren benötigt. Wenn also Bobs Private Key bekannt wird, so
    kann Bob einfach einen neuen erstellen. Seine bisher empfangenen Daten können weiterhin genutzt
    werden. Problematisch wird es nur, wenn die übermittelten Datenpakete noch in verschlüsselter
    Form vorliegen.

## Zertifikate

Für Public / Private Key können auch problemlos Zertifikate genutzt werden. Diese dürfen auch self
signed sein, sofern es die Unternehmenspolicy dies zulässt. Dadurch hat man eine Alterung und
Kontrolle der Zertifikate inbegriffen.

Forscher können auch ihre eigenen Zertifikate bereitstellen, wenn das Unternehmen eins bereitstellt
(das MPI tut dies).
