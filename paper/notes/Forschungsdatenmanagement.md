# Forschungsdatenmanagement

- Globale eindeutige IDs (GUID oder Link)
- erweiterte Metadaten (Videos an Timeline, Bildbereiche)
- Metadaten müssen über eine API erreichbar sein
    - mit Authentifizierung und Authorisierung
- Metadaten sind verfügbar, selbst wenn die Daten nicht mehr verfügbar sind
- Versionskontrolle
    - Ausgabe in git?
    - Zeitstempel nach ISO-8601
- Visualisierung

- FAIR (go-fair.org)

- deutsche Gesellschaft für Sozial- und Kulturanthropologie (DGSKA): erster Release 2015
- mehrsprachiger Zugang
- Lizenz
- Automatische Erzeugung von Datamanagement-Plan, oder Erzeugung von Abschnitten dafür
    (Erleichtert die spätere Arbeit)

- SEHR HOHER SCHUTZBEDARF
