# Database

Dacryptero can hold more than one database. Each of these has there own schema, which describes the
format of the data, and access management.

As long as a database is unlocked to a session you can search for something in any database or
inspect their content. Every database is split in two parts: normal database and confidential
database. The later one is for specific data that you can remove from the system at any time.