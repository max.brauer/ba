## Creating a Database

Creating a database is straight forward:

1. Open the **Home** page of Dacryptero.
2. Click on the **Create Database** button in the first section.
3. In the new menu enter a new database new. You can only insert a name
    - if it is not used before and
    - if it consists of small letters, numbers, underscores and dashes.
    
    If you insert invalid letters they will automatically converted for you. Keep in remind: It is
    not that easy to change the name later!
4. Select a schema from the list. This will define the data format for the new database. Keep in
   remind: It is impossible to change this afterwards!
5. Click on **Create Database**.
6. Click on **Show Master Key**. This will open a new tab and show the master key. Save this
    securely!

    > It is recommended to print and store this in a safe place. Also download it and safe it
    > securely! It is impossible to retrieve this Master Key afterwards. No Backup - No Mercy!

    This master key is used to retrieve lost access to the database. Confirm after saving the master
    key securely with **I have save the Master Key securely**.
7. Click on **Add new User**. Insert any username and password. It is allowed to use them twice.
    Afterwards you are required to insert the FIDO USB Key and confirm the registration.
8. After adding a new user you can add a second or third one. Just repeat step 7.
9. Click on **Create**.
10. Your database is added to the list. You can now use it.

