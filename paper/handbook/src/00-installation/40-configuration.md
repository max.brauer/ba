## Configuration

In the program directory exists a single `config.ini` file. These holds the configuration for
Dacryptero. To change this open it in your text editor and restart Dacryptero after saving your
changes.

If a configuration key can hold a file path then these are allowed to use environment variables. The
environment variables are enclosed in percent signs `%`. For example

```ini
path="%APPDATA%\\Dacryptero\\db"
```

will point to `C:\Users\user\AppData\Dacryptero\db` if the current username is `user`.

> Microsoft has a list of common environment variables for specific folder paths
> [here](https://docs.microsoft.com/en-us/windows/deployment/usmt/usmt-recognized-environment-variables).

If a configuration key can accept text and the text contains back slashes `\\`, then these have to
be escaped. Escaping works if you add another back slash in front of it. This is the case if you
want to use file paths on windows systems. (See example above).

### Config Path

A single configuration can point to another directory which can hold multiple other configurations.
There settings will be read afterwards and overwrite the current settings. In the other directory
online the direct files will be read and sub-directories ignored. Also recursive reading of same
configuration files are ignored.

```ini
config.include.dir="%APPDATA%\\Dacryptero\\configs"
```

### Database Path

> If you install Dacryptero using this guide you have to change the database path!

The database path holds all databases that you will create or use in the future. It is required to
set this to a path where the current user can read and write to. It is recommended to do not place
this on a network share because this can slow down the process.

```ini
db.baseDir="%CSIDL_MYDOCUMENTS%\\Dacryptero\\db"
```

### Schema Path

The schema files are files that describe how the data in a database should be structured and what is
allowed and what not. A schema file is a single file that can be in any subdirectory of the schema
directory. These files are only read at the start of the application.

```ini
db.baseDir="%APPDATA%\\Dacryptero\\configs"
```

### Server configuration

The server holds some configurations that define how the server will contact the user interface in the browser and vice versa. You should not change these settings normally except you have to prepare a special setup. What these parameters do is described in the provided configuration file.

### Other configurations

It is advised to do not change any of the other settings except you know what you do. These can break the security setup or make it difficult to let the support know what your issues are.

