## Run

> This topic is in subject to change in the future.

To open Dacryptero you need to start the background server. This will open the server window and
display some debug output.

After that you need to open `http://localhost:8015/` in your browser (Firefox or Chromium). This
will automatically connect to your server and you can use Dacryptero.

If you close the background server all connections will be cut and the data is stored. Closing
the browser window will not close Dacryptero!

### Session

Dacryptero is session based. Each browser tab or window holds its own session and need to login
separately. **Dacryptero does not use any Cookies!** and therefore cannot transmit a session from
one tab to another. If you refresh or reload a tab the old session will be closed and a new one
starts.
