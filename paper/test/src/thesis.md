# Abstract

# Reading Guide

Chapter about how to read the paper

* Mention Symbol Table
* Glossar
* Explain complicated chapters like ILP (or maybe just at the beginning of the relevant chapters)
* target audience

# Basics

## Graph Theory

## Probablistic Models (Bayesnet)

* additional properties
* structure learning
* notes on choice

## Hypergraph

## mip / ilp

### Basics

### MIP vs ILP

We call it MIP even if it's an ILP. Not using differentiate language here.

### Dynamic Constraints

## Equivalences

* acylicity
* total or half ordering
* transitivity

# Different Viewing Angles

The Problem this paper is addressing can be interpreted differently. There are also some related problems of other research fields:

* DB: acyclicity in sql querries for data bases
* math: finding optimal Hypergraphs
* Bio Informatics: structure learning bayesian networks
* CO: qTSP (with additional requirements)


## the main problem

* quadratic tsp
* hypergraph optimization
* structure learning bayesian networks

## the additional twist

* restricted bandwith
* linear graph layout
* total ordering

## a bit more relaxed

* restricted relaxed bandwith
* linear graph layout (but nodes can be on the same level)
* halfordering

## a different approach

* distance between nodes is longest alternative path
* this does not keep the property, that all paths to a node are of equal length

# MIP / ILP

# Methodology

# Approaches

Often there are multiple ways of modeling a constraint, different solving algorithms (variations from the normal solving rutine) or just minor strategy changes.  
This chapter shows these variants and presents the full range of choices as a parametized model and algorithm.

## List of Possible Variations

(refactor later)

* acyclicity through dynamic constraints or transitivity
* ranks trough z + p vars + z is transitive or z + p vars + acyclicity of z (dynamic constraints)

## List of Possible Cutting Planes

## List of Possible additional Constraints

## From TSP to Structure Learning Bayesian Networks

* prominent TSP technique: subtour elemination constraint
* this could be used to force acylicity and therefore a total or half ordering
* total or half order is the difference between the bandwith and our relaxed bandwith

## Solving and Model Variants

### One Chapter for Each

Explenation

#### Compatibility With Other Variants

#### Evaluation

link to Evaluation chapter of this variant


### Comparison (of All Variants)

# Evaluation

## Single Variants

### Using Cycle Elemination for Acyclicity

* Don't forbid cycles in the graph
* **if** cycle is found **then**  
    add contraints to remove cycle (different variations of this will be discussed later  
  **else**  
    graph is acyclic, and therefore optimal

<!-- \includegraphics[scale=.5]{plots/median.png} -->

## Comparison

## Other Insights

* correlation of runtimes between different variants
* number of user-callbacks
* size distribution of added cycle constraints

## Conclusion

# Future Work

**Idea**  
Rough explenation why it is useful or how it can create more insight

# Appendix
## Notes for Literature

Give overview of used literature and some interesing notes why which paper is important. The Papers for flavour (i.e. "first" paper for sec approach) don't need to be explained here.

## Symbol Table

## Glossar

## Notes for Bad Ideas

These are ideas just briefly tested, which haven't worked.

* binary search
* creating lower bounds
* setting accuracy (computing it was hard)
* quadratic score function does not work
* using dynamic cycle constraints at LP nodes (seams to not work)

## Ideas to Test

* use special implication constraints (gurobi specific)
* quadratic constraints
* SOS constraints
* positions by using quadratic variables:  
  $$ p_{i,j} = 1 \Leftrightarrow \text{vertex }i\text{ is at position }j $$
  $$ \sum p_{i,\bullet} = 1 \qquad \forall\ i$$
  possible cutting plane:  
  $$ \sum p_{\bullet,j} \leq n - \sum\limits_{k < j} p_{\bullet,k} $$

# Nodes for all Chapters

* each chapter begins with brief content description

# Just a test chapter

Using this \parencite{test} fictional work, I managed to prove my work.  
Example plot:  
<!-- ![median of running times](plots/median.png)   -->
