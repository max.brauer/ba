# Anleitung zum Start des Programms von CD

1. Installation von .NET 6
    - [https://dotnet.microsoft.com/en-us/download/dotnet/6.0](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
    - Download und Installation von `.NET Runtime 6.0.6` oder neuer
2. Kopieren des Ordner `/bin` von der CD auf den Rechner. Der neue Ordner muss ein les- und
   schreibbarer Ordner sein. Dies sind im Normalfall alle Nutzerordner.
3. Start der Start-Skripte.
    - Windows: `start.bat`
    - Linux: `start.sh`
    - Andere: Einfach in `start.sh` reinschauen und den Bedingungen anpassen.
4. Navigieren im Browser auf die Seite [http://localhost:8015/](http://localhost:8015/)

Es ist zwingend notwendig, dass `dotnet` über die `PATH` Umgebungsvariable verfügbar ist. Dies ist
bei einer normalen Installation immer der Fall. Notfalls muss der Rechner neu gestartet werden.

## Speicherung von Daten

Im Programmordner werden in der mitgelieferten Konfiguration keine Daten gespeichert. Dennoch werden
manche Daten schreibend geöffnet, obwohl dies eigentlich gar nicht nötig ist. Dies ist ein bekannter
Bug.

Ansonsten werden alle angelegten Dateien im folgenden Nutzerordner gesammelt:

- Windows: `%APPDATA%/Dacryptero`
- Linux: `$HOME/Documents/Dacryptero`

Dies ist in der mitgelieferten Konfiguration so eingestellt. Es ist zwingend notwendig, dass dieser
Ort schreibend verfügbar ist.

Folgende Daten werden im Nutzerordner hinterlegt:

- Zusätzliche Konfigurationen. Wird für die Demonstration nicht benötigt.
- Datenbanken im `db` Ordner.
- Logs im `logs` Ordner. Diese enthalten ausführlichere Angaben, als die Konsole.

## Zwei-Faktor-Authentifizierung

In der mitgelieferten Konfiguration ist die Zwei-Faktor-Authentifizierung **deaktiviert**! Um diese
zu aktivieren, muss in der Konfiguration die Option `security.2fa=true` gesetzt werden. Bei einer
aktivierten Zwei-Faktor-Authentifizierung ist die Nutzung eines Fido2 kompatibles Gerät zwingend
erforderlich.

Die Option `security.2fa` wird in einem zukünftigen Release entfernt. Es wird dann nicht mehr
möglich sein, die Zwei-Faktor-Authentifizierung zu deaktivieren.
