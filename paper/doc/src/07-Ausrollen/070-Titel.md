\cleardoublepage

# Rollout

Dies ist der Weg, um die Anwendung vom Entwicklungsstadium bis zum Endnutzer zu bringen. Dies
beinhaltet auf der einen Seite die Installation beim Nutzer an sich und zum anderen der Weg, den
eine Änderung bei der Anwendung (ein neues Feature, eine Fehlerkorrektur, ...) macht, um dann beim
Nutzer anzukommen.

