## Nachnutzung

Derzeit ist eine Nutzung am ethnologischen Institut der Max-Planck-Gesellschaft geplant und dafür
war auch das Projekt erstellt wurden. Während der Entwicklung zeigten sich schon früh Bedarf und
Interesse an dem Produkt und wünschte sich möglichst früh erste Ergebnisse zu sehen. So kam es auch
zu einen regen Austausch zwischen dem Autor und den Bedarfsträgern bzw. Forschern, welche auch als
erste Testgruppe fungierten.

Nach dem Abschluss des Projekts wird mit dem Auftraggeber und den Bedarfsträgern über den Erfolg
erneut diskutiert und besprochen, wie die weitere Entwicklung (siehe \ref{082-erweiterungen})
geschehen und das Produkt eingesetzt wird.

Eine Verbreitung an die anderen Institute ist derzeit noch nicht vorgesehen, aber in einem
zukünftigen Stadium geplant.
