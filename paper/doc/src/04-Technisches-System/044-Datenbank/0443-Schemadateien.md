### Schemadateien

Schemadateien sind ein Konzept, welches zusätzlich eingeführt wurde, um die Daten in der Datenbank
genauer zu spezifizieren und an die Bedürfnisse des Forschers und dessen Projekt anzupassen. Hierbei
wird das Datenbankschema nicht verändert. Die Datenbank speichert seine Daten weiterhin im sehr
allgemeinen Format. In der Schemadatei wird eine Liste von Rollen angegeben, die eine befragte
Person haben kann. Zu jeder Rolle wird zusätzlich eine Liste von Attributen spezifiziert und
angegeben, welchen Typ diese haben und wie diese verwaltet oder bearbeitet werden sollen.

Alle Schemadateien sind JSON Dateien und befinden sich in einem vom Nutzer konfigurierten Ordner. In
diesem können sie in einem beliebigen Unterordner befinden. Alle JSON Dateien darin werden von der
Anwendung als Schemadateien betrachtet. Bei der Namensgebung der Pfade haben das Institut und der
Nutzer freie Wahl. Einzig `/internal/**/*.json` wird für interne Testzwecke für die Zukunft
reserviert.

Der Aufbau der Schemadatei wird im Anhang \ref{9940-rollenschema} genauer erklärt. Jede Schemadatei
enthält eine Liste von Rollen mit Attributen, die von oben nach unten ausgewertet werden. Jede
Person in der Datenbank (das ist der Datensatz zu einer befragten realen Person), welche vom
Forscher in der Anwendung angelegt wird, entspricht genau eine dieser Rollen, welche derzeit nicht
nachträglich geändert werden kann.

Falls mehrere Rollen gleiche Attribute enthalten, kann eine neue Rolle (Eltern-Rolle) angelegt
werden, welche die gleichen Attribute enthält, und alle Rollen erben dann von der neuen Rolle.
Dadurch übernehmen alle Kinder-Rollen (das sind die Rollen, die von der Eltern-Rolle erben), alle
Attribute der Eltern-Rolle. Jede Rolle kann nur von einer anderen Rolle direkt erben, aber eine
Rolle kann von einer Rolle erben, die schon von einer anderen Rolle erbt.

Eine Rolle kann nur von einer anderen Rolle erben, die im Schema vorher definiert wurde. Dadurch
wird das Problem mit der zyklischen Vererbung umgangen. Falls die Eltern-Rolle nicht vom Forscher
direkt genutzt werden soll, dann empfiehlt es sich, diese Eltern-Rolle als abstrakt zu markieren.

Jede Rolle hat eine Liste von Attributen. Ein Attribut ist eine Eigenschaft, die der Nutzer bei der
Person einstellen kann. Diese bekommt exakt den Namen, der im Schema als Schlüssel definiert wurde.
Als Typ lässt sich hier die Art des Editors bestimmen, welche wiederum auch den Wertetyp definiert
(siehe Tabelle \ref{schema-editortypen}). Eine genauere Einschränkung für den Editor gibt es derzeit
nicht.

\begin{mytable}{Editortypen}{schema-editortypen} \centering
\begin{tabulary}{1.0\textwidth}{|l|l|L|}
    \hline
    Editortyp & Wertetyp & Beschreibung \\
    \hline
    \hline
    \texttt{DateTime} & \texttt{string}, \texttt{<date-time>} & Angabe von Datum \textbf{und} Zeit.
    Angezeigt wird in lokaler Zeitzone des Anwenders. Gespeichert in UTC. \\
    \hline
    \texttt{SingleLine} & \texttt{string} & einzeiliger Text \\
    \hline
    \texttt{MultiLine} & \texttt{string} & mehrzeiliger Text \\
    \hline
    \texttt{Number} & \texttt{number} & Fließkommazahl \\
    \hline
    \texttt{Int} & \texttt{int} & Ganzzahl \\
    \hline
    \texttt{Bool} & \texttt{bool} & Checkbox mit Ja, Nein \\
    \hline
\end{tabulary}
\begin{footnotesize}
    Alle Editortypen für Attribute, die von der Oberfläche unterstützt werden.
\end{footnotesize}
\end{mytable}

Weiterhin lässt sich zu jedem Attribut sagen, ob dies protected ist, also in der Confidential
Datenbank gespeichert wird. Diese Einstellung ist durch das Schema vorgegeben und lässt sich nicht
im Nachhinein ändern. Sobald die Confidential Datenbank entfernt wurde, kann der Wert nicht mehr
eingesehen werden, aber auch eine Bearbeitung ist nicht mehr möglich.

Außerdem lässt sich für jedes Attribut festlegen, ob es eine Historie anlegen soll. Falls diese
Option aktiviert ist, wird jeder historische Zustand in der Datenbank abgespeichert und lässt sich
durch den Nutzer ansehen. Ist die Option deaktiviert, so ist immer nur der aktuelle Wert zu sehen
und eine Bearbeitung ist nicht möglich.

\begin{myfigure}{Rollenschema für das Projekt}{rollenschema} \centering
\includegraphics[scale=0.25]{role.v1.jpg} \\
\begin{footnotesize}
    Dies ist eine grafische Visualisierung wie das Rollenschema für das Forschungsthema des
    Auftraggebers aussieht. In kursiv sind die Namen von abstrakten Rollen gekennzeichnet. Die
    Pfeile geben an, von welcher Rolle diese ihre Attribute erben. Bei den Attributen wurde nur
    der Wertetyp angegeben. Die Schemadatei ist im Quellcode unter
    \texttt{/server/Dacryptero/schemas/eth/dev/v1.json} zu finden.
\end{footnotesize}
\end{myfigure}

In Abbildung \ref{rollenschema} wird das Rollenschema, welches für das Forschungsthema des
Auftraggebers erstellt wurde, dargestellt. Hier ist deutlich zu erkennen, dass je nach Rolle
unterschiedliche Informationen abgefragt und gespeichert werden.
