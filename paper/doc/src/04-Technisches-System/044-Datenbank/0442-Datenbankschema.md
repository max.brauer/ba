### Datenbankschema

Das Erstellen des Datenbankschemas stellte sich als schwierig heraus. Das größte Problem war, dass
zuerst unklar war, was die Auftraggeber erwarten und welche Daten diese benötigen. Dabei entstanden
verschiedene Iterationen des Datenbankschemas. Im späteren Verlauf haben sich Autor und Auftraggeber
darauf geeinigt, das Datenbankschema allgemeingültig für verschiedene Forschungszwecke zu machen und
dann über ein separates Schema dies zu konfigurieren und einzuschränken (siehe
\ref{0443-schemadateien}).


\begin{myfigure}{Entity Relationship Diagram}{db-schema-er} \centering
\includegraphics[scale=0.19]{data.v8.png} \\
\begin{footnotesize}
    Diese Abbildung gibt es noch einmal im Anhang \ref{990-entity-relationship-diagramm-v0.6} als
    große Grafik. \newline
    \newline
    Dies ist ein Entity Relationship Diagram in der Krähenfuß-Notation (siehe \cite{8756667}) oder
    auch Martin-Notation (\cite{wiki-martin-notation}). Die Krähenfüße geben die Kardinalitäten der
    Beziehungen an. So ist in diesem Beispiel ein \texttt{File} mit $0-n$ \texttt{FileEntry} und ein
    \texttt{FileEntry} mit $1$ \texttt{File} verbunden. Ein \texttt{Attribute} ist mit $1-n$
    \texttt{AttributeEntry} und über \texttt{Recent Entry} ist ein \texttt{AttributeEntry} mit $0-1$
    \texttt{Attribute} verbunden.
\end{footnotesize}
\end{myfigure}

Was sofort am Schema auffällt, ist die Aufteilung in verschiedene Bereiche. Es gibt hier die Main
Database und die Confidential Database. Die Aufteilung der beiden ergibt sich aus den Anforderungen
siehe \ref{0323-schutz-der-betroffenen}. Beide enthalten zusammen die Datensätze, die der Forscher
im Laufe der Forschung eingibt. Diese Datensätze sind je nach Priorität entweder in der Confidential
Database oder in der Main Database. Die Anwendung braucht auf jeden Fall die Main Database, um mit
den Daten zu arbeiten. Die Confidential Database kann zu einem beliebigen Zeitpunkt entfernt oder
wieder hinzugefügt werden.

Zentrale Objekte in der Datenbank ist die `Person`, `Interview`, `File` und `Attribute`, welche alle
in Beziehung zueinanderstehen. Alle vier Objekttypen finden sich in beiden Datenbanken wieder.
Einzig `Interview` ist nur in der Confidential Datenbank enthalten. Von `Person` gibt es immer zwei
Varianten, welche jeweils in der Main Database und in der Confidential Database enthalten sind.
Beide Varianten sind über die gleiche Id verbunden.

`File` und `Attribute` sind als Objekte nur in einen von beiden Datenbanken enthalten, je nachdem,
ob sie derzeit confidential deklariert wurden oder nicht. Jeweils von `File` und von `Attribute`
wird eine Historie angelegt, welche sich auch im Datenbankschema widerspiegelt. Somit ist der
Bearbeitungsverlauf der Daten immer ersichtlich.

Das Verhalten und die Nutzung von `Attribute` ist in Kapitel \ref{0443-schemadateien} näher
erläutert.

Die Objekte von `File` enthalten nur die Metainformationen zu den Dateien. Die Inhalte selbst werden
parallel zu den Datenbankdateien verschlüsselt gespeichert. Der Schlüssel für jede Datei ist
einzigartig und für jede neu generiert. Diese werden alle in die Datenbank mitgespeichert.

Zu dem oben genannten Schema wurden dann C# Klassen erstellt, die dem Schema entsprechen. Diese sind
im Quellcode im Ordner `/server/Dacryptero/Data` zu finden. LiteDB selbst kümmert sich mittels
Reflection automatisch um die Übersetzung zwischen der internen BSON Repräsentation und den Daten in
den C# Klassen (siehe Beispiel in Kapitel \ref{0441-litedb}). Mehr Informationen dazu, wie das in
LiteDB funktioniert, findet sich hier \cite{litedb-object-mapping}.
