### Front-End (UI)

Für das Front-End wurde eine moderne HTML-Oberfläche gewählt. Diese hat den Vorteil, dass sich diese
auch später ohne großen Aufwand auf andere Plattformen oder Systeme übertragen lässt. (Webbrowser
sind fast überall verfügbar.) Außerdem hat sich das Web mittlerweile so weit entwickelt, dass viele
Office Tätigkeiten sich auch heute schon komplett im Browser erledigen lassen (z. B. Dokumente
schreiben, E-Mails lesen, einfache Videobearbeitung, ...) und auf Spezialanwendungen auf dem Rechner
verzichtet.

Als Programmiersprache für die UI hat der Autor die Sprache Elm gewählt (für den Vergleich siehe
Kapitel \ref{0231-front-end-ui}). Das Styling wird durch handgeschriebenes CSS gemacht. Hier wird
kein Framework genutzt.
