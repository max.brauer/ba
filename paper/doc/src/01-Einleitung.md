\cleardoublepage

# Einleitung

Diese Arbeit wurde am Max-Planck-Institut für ethnologische Forschung (MPI) und dem Institut der
Informatik der Martin-Luther-Universität Halle-Wittenberg erstellt. Das Ziel ist das
Forschungsdatenmanagement von ethnologischen Instituten (in diesem Fall das MPI) mithilfe aktueller
Techniken und Software zu verbessern.

\begin{quote}
    "Das Max-Planck-Institut für ethnologische Forschung ist eines der führenden Forschungszentren
    auf dem Gebiet der Ethnologie. Forschungsleitend ist die vergleichende Untersuchung
    gegenwärtiger sozialer Wandlungsprozesse. Auf diesem Gebiet leisten die Wissenschaftlerinnen und
    Wissenschaftler des Instituts einen wichtigen Beitrag zur ethnologischen Theoriebildung. Sie
    befassen sich darüber hinaus in ihren Projekten oft auch mit Themen, die im Mittelpunkt
    politischer Debatten stehen." (Quelle: \cite{mpi-webseite-text})
\end{quote}

Die Forscher des MPI begeben sich bedingt durch ihre Tätigkeit auf weltweite Reisen, um mit einer
großen Vielfalt an Personen Interviews zu führen und Daten für ihre Forschung zu sammeln.
Hauptsächlich werden diese Interviews in Papierform (handschriftliche Notizen) oder als einfache
Audioaufnahmen dokumentiert. Letzteres wird in viel manueller Arbeit als Textdatei transkribiert und
alles zusammen in einer großen Sammlung an Word-Dokumenten abgespeichert und archiviert.

Eine weitere Verarbeitung mit diesen Dokumenten erfolgt dann nur noch über das Programm Microsoft
Word und/oder mit den Ausdrucken in Papierform. Diese Dokumente erfahren häufig keine richtige
Versionierung (Änderungen sind nicht mehr oder schwer nachvollziehbar) und spätestens beim Teilen
mit Forschungskollegen entstehen Duplikate, die später nur sehr aufwendig händisch vereinigt werden.

Ein weiterer wichtiger Aspekt ist die Sicherheit der aufgenommenen Daten. Die Daten waren bisher
ungesichert auf Papier oder auf elektronischen Datenträgern hinterlegt und es war bisher schwierig,
diese vor unbefugten Einsehen oder vor Zerstörung zu schützen. Dies kann mitunter an
Grenzübertritten in Staaten wie China (\cite{sz-china-app} und \cite{zeit-china-grenze}), USA (siehe
Absatz \dq{}Besondere Sicherheitsmaßnahmen\dq{}, \cite{auswaertiges-amt-usa}) und Neuseeland (siehe
Absatz \dq{}Einfuhrbestimmungen\dq{}, \cite{auswaertiges-amt-neuseeland}) geschehen, bei denen das
Gepäck inklusive Datenträger oder Computer/Mobiltelefone durchsucht, einbehalten oder mit Trojanern
versehen werden. In dieser Arbeit wird dieser Aspekt der Sicherheit sehr hoch angesehen und es
wurden verschiedene Leitlinien für den sicheren Umgang mit Daten umgesetzt.

Zur Sicherheit gehört auch der Datenschutz selbst, welcher gesetzlich durch das europäische
Datenschutzgrundgesetz (DSGVO, GDPR) und durch das Bundesamt für Sicherheit und Informationstechnik
(BSI) geregelt ist. Nur berechtigte Personen sollen Zugang zu den Daten haben, diese einsehen oder
modifizieren können. Dazu wurde in erster Linie eine Zwei-Faktor-Authentifizierung eingeführt. Zum
Datenschutz gehört es auch, dass bestimmte Daten ab einen bestimmten Zeitpunkt nicht mehr für die
Forschung relevant und deshalb anonymisiert, das heißt für den Forscher nicht mehr zugänglich sein
sollen. Als Beispiel lassen sich hier besonders schützenswerte Daten wie Name, Kontakt,
Religionsangehörigkeit und Gesundheitsstatus der Befragten nennen. Außerdem sollen bestimmte Daten
mit Forschungskollegen auf sicheren Wegen geteilt werden können, bei dem die Anonymisierung und der
Datenschutz nicht verletzt werden.

Ein strukturiertes Abspeichern der Daten und eine schnelle Suche in diesen ist der zweite große
Punkt, der in dieser Arbeit verfolgt wird. Dem Forscher soll es möglich sein, seine Daten schnell,
einfach und sicher eingeben zu können und sehen, was sich wann geändert hat. Dadurch wurde auch der
Grundsatz der Versionierung berücksichtigt.

Der letzte große Punkt ist das einfache Teilen und Erstellen eines Back-ups der Daten selbst. Diese
wurde mit Imports und Exports der Datenbanken selbst geregelt.

In dieser Arbeit kommen unterschiedliche moderne Techniken zum Einsatz. Dazu zählen das
Datenbankmanagementsystem LiteDB, die Programmiersprachen C# und Elm, eine moderne Oberfläche im
Webbrowser, die 2-Faktor-Authentifizierung mittels WebAuthn und Fido2 und Datensicherheit und
Verschlüsselung per Design.

Diese Arbeit ist um den Entwicklungsprozess der Anwendung strukturiert. Im ersten Kapitel nach
dieser Einleitung geht es um die inhaltlichen und technischen Anforderungen, die an die Anwendung
gestellt werden und eine Auswahl der Komponenten wird getroffen. Im nächsten Kapitel geht es um die
Sicherheit, den Datenschutz und die technische Basis dazu. Danach wird das technische System
entworfen und die einzelnen Komponenten werden genauer beleuchtet. Schließlich geht es um die
Umsetzung und die Stolpersteine, die dabei aufgetreten sind. Im darauffolgenden Kapitel geht es um
die Tests, welche genutzt wurden und welche Vor- bzw. Nachteile dabei auftraten. Anschließend geht
es darum, wie die Anwendung an alle Nutzer ausgerollt wird und welche Zyklen dabei durchlaufen
werden. Nach diesem Kapitel kommt dann der Ausblick, in dem die Nachnutzung, Erweiterungen, Wartung
und Updates genauer beleuchtet werden. Das letzte Kapitel enthält das Fazit der Arbeit.
