## Inhaltliche Anforderungen

Die Anwendung soll die Daten, welche bei den Forschungsreisen und Interviews eines Forschers
anfallen, aufnehmen, übersichtlich darstellen und organisieren. Zu diesen Daten gehören Folgende:

- **Informationen über den Interviewpartner**
  
    Dazu zählen Kontaktinformationen wie Name, Adresse und Telefonnummer. Außerdem müssen je nach
    Interviewpartner auch bestimmte Dokumente hinterlegt werden, wie die unterschriebene
    Datenschutzerklärung.
- **Arten der Interviewpartner**

    Diese sind je nach Forschungsthema unterschiedlich und können zum Beispiel Patient, Arzt,
    Krankenpfleger, familiäre Angehörige und religiöse Beistehende sein.
- **Familiäre Beziehungen zwischen den Interviewpartnern**

    Dies ist je nach Forschungsthema unterschiedlich, ob diese überhaupt aufgenommen werden sollen
    oder dürfen.
- **Informationen wann und wo ein Interview stattgefunden hat und wer dort interviewt wurde**
- **Antworten auf bestimmte Fragen**

    Dies ist vom Forschungsthema abhängig und die Antworten darauf können sehr unterschiedlich
    ausfallen. Die Fragen sind zudem von der Art des Interviewpartners abhängig.
- **Dateien zu den Interviewpartnern**

    Das können Familienfotos, Tonbandaufnahmen, Fotos vom Wohnhaus oder Skizzen sein. Hier ist die
    Art der Dokumente je nach Interviewpartner unterschiedlich. Zum Teil lassen sich auch mehrere
    Dateien gruppieren.

Ein Teil der Daten unterliegt besonderen Regeln des Datenschutzes (besonders schützenswerte Daten
wie Name und Kontaktinformation) und dürfen zu bestimmten Zeitpunkten nicht mehr für die Forschung
genutzt werden. Diese müssen anonymisiert und Daten, welche auf Personen zurückschließen lassen,
entfernt werden. Die entfernten Daten müssen an einen sicheren Platz für eine spätere Einsicht aufbewahrt
werden. Dies betrifft zum Beispiel den Klar-Namen und Kontaktdaten des Betroffenen. Für die Aufnahme
und Durchführung des Interviews sind diese Daten noch relevant. Bei der Auswertung der Daten sollen
diese nicht mehr verfügbar sein und spätestens bei der Veröffentlichung darf nichts mehr enthalten
sein, was eine spätere Identifikation der Person ermöglicht (z. B. "Chefarzt im Krankenhaus der Stadt
X, welcher einen Schnurrbart trägt, ..."). Die betroffene Person kann später immer die Löschung der
eigenen Daten verlangen. Dafür werden wieder die Kontaktdaten und eine Zuordnung, welche
anonymisierte Daten dadurch betroffen sind, gebraucht. Daher ist ein einfaches Löschen der
Zuordnungen nicht möglich.

In der Praxis werden Zuordnungstabellen zu den Kontaktinformationen erstellt. Diese werden dann in
gedruckter Form oder als digitales Medium in einem sicheren physischen Tresor verwahrt. Für die
Forschung steht die Person dann nur noch als anonyme ID zur Verfügung.
