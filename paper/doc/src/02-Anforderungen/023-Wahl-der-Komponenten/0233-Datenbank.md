### Datenbank

Die Datenbank ist ein kritisches Thema, da sie alle sensiblen Daten des Forschers enthält und
speichern muss. Gleichzeitig reicht es für die Datenbank nicht aus alle Daten einfach zu speichern,
sondern diese muss diese Daten auch in angemessener Zeit wieder bereitstellen, damit die Daten
weiterverarbeitet werden können. Dies kann eine einfache Anzeige der Daten oder eine Suche nach
bestimmten Datensätzen sein.

Daher wurden folgende Kriterien für die Auswahl des Datenbankenmanagementsystems (DBMS) festgelegt:

1. Sicherheit: Die Daten müssen verschlüsselt auf der Festplatte vorliegen. Auch der Schlüssel muss
   sicher sein.
2. Zugriff: Der Server braucht einen schnellen und einfachen Zugriff auf die Daten. Dies bedeutet,
   dass die API einfach für den Server und Programmierer zu nutzen und zu verstehen sein muss.
   Gleichzeitig müssen die Daten in angemessener Zeit verfügbar sein, damit nicht lange Wartezeiten
   verursacht werden.
3. Back-ups: Sicherheitskopien müssen sich leicht erstellen lassen.
4. Angriffsoberfläche: Je mehr Funktionen ein DBMS liefert, desto mehr Schwachstellen kann diese
   haben. Diese müssen alle einzeln abgesichert werden. Das kann erhöhten Konfigurationsaufwand für
   den Anwender bedeuten. Weniger oder das komplette Deaktivieren nicht benötigter Funktionen kann
   eine Erleichterung für die Einrichtung und Verwaltung einer Datenbank bedeuten.
5. Lizenzen: Die Lizenz muss mit dieser Arbeit kompatibel sein. Daher ist es schwierig,
   kommerzielle Lizenzen zu nutzen.

\begin{mytable}{Übersicht der DBMS}{uebersicht-dbms} \centering
\begin{tabulary}{1.0\textwidth}{|*{6}{L|}}
   \hline
   Kategorie & \href{https://mariadb.org/}{\mbox{MariaDB}}
      & \href{https://www.mysql.com/de/}{\mbox{MySQL}}
      & \href{https://sqlite.org/index.html}{\mbox{SQLite}}
      & \href{https://www.mongodb.com/de-de}{\mbox{MongoDB}}
      & \href{https://www.litedb.org/docs/}{\mbox{LiteDB}} \\
   \hline
   \hline
   Webseite & \texttt{mariadb.org} \cite{dbms-mariadb-home}
      & \texttt{mysql.com} \cite{dbms-mysql-home}
      & \texttt{sqlite.org} \cite{dbms-sqlite-home}
      & \texttt{mongodb.com} \cite{dbms-mongodb-home}
      & \texttt{litedb.org} \cite{dbms-litedb-home} \\
   \hline
   Relational & x & x & x & & \\
   \hline
   Art & server\-basiert & server\-basiert & embedded & server\-basiert & embedded \\
   \hline
   Lizenz
      & GPL 2.0
      & GPL 2.0\textsuperscript{*}
      & public domain
      & \href{https://github.com/mongodb/mongo/blob/master/LICENSE-Community.txt}{SSPL v1} \cite{dbms-mongodb-license}
      & MIT \\
   \hline
   Open Source
      & \href{https://github.com/MariaDB/}{GitHub} \cite{dbms-mariadb-source}
      & \href{https://github.com/mysql/mysql-server}{GitHub} \cite{dbms-mysql-source}
      & \href{https://sqlite.org/src/dir?ci=trunk}{Sqlite.org (Fossil)} \cite{dbms-sqlite-source}
      & \href{https://github.com/mongodb/mongo}{GitHub} \cite{dbms-mongodb-source}
      & \href{https://github.com/mbdavid/litedb}{GitHub} \cite{dbms-litedb-source} \\
   \hline
   Preis & free & free\textsuperscript{*} & free & free & free \\
   \hline
   Erster Release & 29.10.2009 & 23.05.1995 & 17.08.2000 & 11.02.2009 & 17.09.2016 \\
   \hline
\end{tabulary}
\begin{footnotesize}
    MySQL ist nur dann kostenlos und unter GPL 2.0, wenn das gesamte Produkt auch unter der GPL 2.0 veröffentlicht wird. Ansonsten handelt es sich um eine kostenpflichtige Lizenz. Siehe \cite{dbms-mysql-license}.
\end{footnotesize}
\end{mytable}

Für den Vergleich wurden MariaDB, MySQL, SQLite, MongoDB und LiteDB einbezogen (siehe Tabelle
\ref{uebersicht-dbms}). Es mag noch mehr Möglichkeiten geben, aber diese fünf sind die
Gebräuchlichsten mit Bibliotheken für C#.

\begin{mytable}{Lokale Verschlüsselung der DBMS}{verschluesselung-dbms} \centering
\begin{tabulary}{1.0\textwidth}{|*{6}{L|}}
   \hline
   Kategorie & \href{https://mariadb.org/}{\mbox{MariaDB}}
      & \href{https://www.mysql.com/de/}{\mbox{MySQL}}
      & \href{https://sqlite.org/index.html}{\mbox{SQLite}}
      & \href{https://www.mongodb.com/de-de}{\mbox{MongoDB}}
      & \href{https://www.litedb.org/docs/}{\mbox{LiteDB}} \\
   \hline
   \hline
   komplette Datenbank
      & \href{https://mariadb.com/kb/en/data-at-rest-encryption-overview/}{supported}
         \cite{dbms-mariadb-encryption}
      & \href{https://www.mysql.com/products/enterprise/tde.html}{nur in kostenpflichtiger Enterprise-Version}
         \cite{dbms-mysql-encryption}an
      & \href{https://github.com/praeclarum/sqlite-net}{alternativer Fork}
         \cite{dbms-sqlite-encryption}
      & \href{https://www.mongodb.com/basics/mongodb-encryption}{aufwendig und nur Felder}
         \cite{dbms-mongodb-encryption}
      & \href{https://www.litedb.org/docs/encryption/}{supported}
         \cite{dbms-litedb-encryption} \\
   \hline
   einzelne Tabellen & supported & & nur komplette Datenbank & nur Felder & nur komplette Datenbank \\
   \hline
   Engines & XtraDB, InnoDB, teilw. Aria & & alle & alle & alle \\
   \hline
   Speicherort der Schlüssel & lokale Datei & zentral in einer Schlüsselverwaltung & frei & frei & frei \\
   \hline
   Methode & AES & AES & AES & AES & AES \\
   \hline
\end{tabulary}
\begin{footnotesize}
   \textbf{MariaDB:} Bei aktivierter Verschlüsselung kann es zu Back-up-Problemen kommen, da externe
Programme die Logs nicht lesen können (mit Ausnahme von MariaDB Back-up). Weiterhin ist der Galera
gcache in der Community Version unverschlüsselt - in der kostenpflichten Version dagegen schon. Die
   Logs \texttt{general query log}, \texttt{slow query log}, \texttt{aria log} und \texttt{error
log} sind unverschlüsselt. Es müssen viele Konfigurationen am Server angepasst und ein
Verschlüsselungs-Plugin installiert werden. Der Verschlüsselungs-Key muss in irgendeiner Art und Weise
als lesbare, unverschlüsselte Datei auf der Festplatte liegen. (siehe \cite{dbms-mariadb-keys})
\end{footnotesize}
\end{mytable}

Punkt 1 ließ sich von keinen DBMS außer LiteDB zufriedenstellend erfüllen (siehe Tabelle
\ref{verschluesselung-dbms}). Zum Teil ist ein enormer Aufbau nötig, der Schlüssel liegt
unverschlüsselt auf der Festplatte, es müssen unbekannte Patches genutzt werden oder es ist nur
möglich, die Werte innerhalb der Zellen zu verschlüsseln. Was auch nicht sein darf, ist, dass Daten
unverschlüsselt in Logs stehen können (MariaDB).

Punkt 2 ist nur bei MongoDB unzufriedenstellend. Bei allen anderen reicht es beim Verbindungsaufbau
den Schlüssel auszutauschen oder zu authentifizieren und danach läuft alles transparent ab. Bei
MongoDB muss dagegen bei jedem Einfügen, Bearbeiten oder Suchen ein Schlüssel übermittelt werden und
die Anfrage muss gleichzeitig den Ver- und Entschlüsselungsprozess beinhalten.

Back-ups von einzelnen Datenbanken lassen sich bei Standalone DBMS schwieriger anlegen. Hier sind die
Daten z. T. an mehrere Orte verteilt und es gibt aufwendige Prozesse, lokale Back-ups
wiederherzustellen. Alternativ ist es möglich, alle Daten in ein allgemein lesbares Format (z. B. SQL)
zu exportieren und später wieder importieren. Dies dauert aber in der Regel deutlich länger als eine
einfache lokale Kopie der Daten. DBMS, die im Prozess der Anwendung laufen, sind dagegen meist so
gestrickt, dass 1-2 lokale Dateien mit allen Daten existieren, welche einfach nur kopiert werden
müssen.

\begin{mytable}{Autorisierung der DBMS}{autorisierung-dbms} \centering
\begin{tabulary}{1.0\textwidth}{|*{6}{L|}}
   \hline
   Kategorie & \href{https://mariadb.org/}{\mbox{MariaDB}}
      & \href{https://www.mysql.com/de/}{\mbox{MySQL}}
      & \href{https://sqlite.org/index.html}{\mbox{SQLite}}
      & \href{https://www.mongodb.com/de-de}{\mbox{MongoDB}}
      & \href{https://www.litedb.org/docs/}{\mbox{LiteDB}} \\
   \hline
   \hline
   Nutzerautorisierung & x & x & - & x & - \\
   \hline
   Mehrere Accounts & x & x & - & x & - \\
   \hline
   Rollen & x & x & - & x & - \\
   \hline
   Rechtemanagement & detailliert & detailliert & - & detailliert & - \\
   \hline
\end{tabulary}
\begin{footnotesize}
   Hier wurde geschaut, ob die jeweiligen DBMS die bestimmten Funktionen haben.
\end{footnotesize}
\end{mytable}

Ein weiterer Nachteil bei Standalone-DBMS ist, dass diese naturgemäß eine größere Angriffsoberfläche
nach außen bieten, da sie eine Vielzahl von Nutzern Zugriff gewährt. Um dies möglichst gut
abzusichern, ist eine Konfiguration des Webservers und Einrichten von Accounts, Rollen und Rechten
notwendig (siehe Tabelle \ref{autorisierung-dbms}). Die getesteten Embedded-Datenbanken weißen
keine dieser besonderen Funktionen auf, da diese in der Regel nur von einer Anwendung genutzt
werden und die sich selbst um die Rechtevergabe kümmern muss. In diesem Projekt wird eine lokale
Datenbank benötigt, auf die nur ein Nutzer gleichzeitig Zugriff hat - und dies ist der Server. Daher
reicht eine Datenbank aus, die im Anwendungsprozess läuft.

Vom Kostenfaktor sind alle bis auf MySQL kostenlos und unter Open-Source-Lizenzen verfügbar. MySQL
ist nur dann kostenlos, falls alles unter GPL 2.0 veröffentlicht wird.

Unter Berücksichtigung aller Punkte wurde sich für LiteDB entschieden, da es als einzige die
Anforderungen an die Sicherheit vollkommen unterstützt.
