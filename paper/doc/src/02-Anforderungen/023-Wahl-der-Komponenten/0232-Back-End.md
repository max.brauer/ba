### Back-End (Server)

Die Anwendung ist modular aufgebaut mit einer Web-Oberfläche und einem dazugehörigen Server. Dieser
kümmert sich um die Verwaltung der Datenbanken, die Verschlüsselung, Verifikation und Back-ups. Das
ist eine vergleichsweise große Palette an Aufgaben. Außerdem muss der serverseitige Teil eine
Schnittstelle zur Oberfläche bereitstellen, damit diese auch Daten austauschen können.

\begin{mytable}{Übersicht Programmiersprachen}{uebersicht-server-programmiersprachen} \centering
\begin{tabulary}{1.0\textwidth}{|*{8}{L|}}
    \hline
    Sprache & C\# & Java & C/C++ & Rust & Java\-Script & Elm & Type\-Script \\
    \hline
    \hline
    Er\-schei\-nungs\-jahr & 2001 & 1995 & 1985 & 2015 & 1995 & 2012 & 2012 \\
    \hline
    Windows & \mbox{nativ} & \mbox{nativ} & \mbox{nativ} & \mbox{nativ} & Node.js - &  & Node.js \\
    \hline
    Linux & \mbox{nativ} & \mbox{nativ} & \mbox{nativ} & \mbox{nativ} & Node.js & - & Node.js \\
    \hline
    Docker & \mbox{nativ} & \mbox{nativ} & \mbox{nativ} & \mbox{nativ} & Node.js & - & Node.js \\
    \hline
\end{tabulary}
\begin{footnotesize}
    Hier wurde geschaut, wie Programme in der Programmiersprache auf dem Rechner ausgeführt werden
    kann. Nativ kann ohne externe Werkzeuge und Node.js nur mit dem Programm Node.js. Bei Docker
    wurde nur geschaut, ob sich dies in einem Dockercontainer ausführen lässt. Zur Anwendung kommt
    dies aber in dieser Arbeit nicht. Bei Elm ist dieser Vergleich nicht anwendbar.
\end{footnotesize}
\end{mytable}

Hierfür wurden verschiedene Programmiersprachen verglichen (siehe Tabelle
\ref{uebersicht-server-programmiersprachen}). Dabei gilt hier, dass die Programme auf dem Computer
des Nutzers und nicht im Web-Browser ausgeführt werden soll. Ein Teil der Programmiersprachen kann
nativ ohne externe Werkzeuge auf dem Zielrechner ausgeführt werden. C# und Java übersetzen hierbei
aber nicht in Maschinensprache, sondern in eine Zwischensprache, die von einer Art virtuellen
Maschine ausgeführt wird. Alle anderen Sprachen, die nach Javascript übersetzen oder es selbst schon
sind, können über das Programm NodeJS direkt auf dem Rechner ausgeführt werden.

Auch hier wurden ein paar Kriterien für die Auswahl definiert:

1. Es muss auf einem Windows- und Linux-Rechner ausführbar sein.
2. Es muss recht schnell und zuverlässig seine Aufgaben ausführen. Das bedeutet, dass die Laufzeiten
   für Aufgaben in einem für den Nutzer angemessen Rahmen sein müssen.
3. Es muss sicher und möglichst ohne Laufzeitfehler funktionieren.
4. Es muss leicht zu debuggen und zu warten sein.
5. Es muss eine gute Dokumentation existieren.
6. Es muss ein großer Umfang an Bibliotheken existieren, um die Aufgaben zu bewältigen.
7. Es muss multi-threaded arbeiten, um die gesamte Rechenleistung des Computers für die Aufgaben
   nutzen zu können.
8. Es muss möglichst weit verbreitet sein, damit das Projekt langfristig gewartet werden kann. Dies
   ist insbesondere für die Daten in der Datenbank wichtig.

Hier stellt sich schnell heraus, dass Elm für diese Aufgaben völlig ungeeignet ist, da diese
Programmiersprache nicht dafür designt wurde. Es ist zwar theoretisch möglich, Programme in Elm auf
dem Rechner ohne Web-Browser ausführen zu lassen, aber dies ist mit enormem Aufwand verbunden, da
hier ein Großteil der nötigen Bibliotheken fehlt (z. B. zum Netzwerk oder zum Dateisystem).

Ansonsten treffen fast alle Kriterien auf alle anderen Kandidaten zu. Punkt 3 lässt sich am besten
bei Rust umsetzen. Hier wird der Entwickler durch Sprache und Compiler dazu gezwungen, den Speicher
sicher und sauber zu halten, und es kommt nicht zu Laufzeitfehlern (was nicht heißt, dass das
Programm abstürzen, quasi "panicken", kann). Dafür ist es aber eher weniger verbreitet (Punkt 8) und hat
eine steile Lernkurve und braucht daher einiges an Einarbeitungszeit.

Der Punkt 7 ist in NodeJS zwar möglich, aber umständlicher zu erreichen als bei den anderen
Kandidaten. Daher eignen sich hier Programmiersprachen, die nicht auf NodeJS angewiesen sind.

Zwar ist es praktisch, wenn der Entwickler eine Programmiersprache hat, die sehr maschinennah
arbeitet (C/C++), um den letzten Funken Geschwindigkeit rauszuholen, aber für dieses Projekt ist es
auch in Ordnung, wenn eine andere genutzt wird. Solange die gesamte Reaktionszeit sich im
akzeptablen Rahmen hält. Da für diese Arbeit die Entwicklungszeit relativ knapp bemessen ist, wird
hier auch eine Programmiersprache bevorzugt, die einfacher und schneller zu schreiben ist.

Unter Berücksichtigung der oben genannten Punkte hat sich der Autor für die Programmiersprache C#
entschieden. Diese ist mit über einer Millionen GitHub Repository (siehe
\cite{github-csharp-repository}) sehr weit verbreitet, arbeitet schnell und zuverlässig und kann
auch in den restlichen Anforderungen gut punkten. Durch den Compiler und die Interpretationsschicht
ist sie gleichzeitig auch so weit von der Maschine abstrahiert, dass es mit ihr möglich ist, schnell
Code zu schreiben, ohne auf die zugrunde liegende Maschine genauer eingehen zu müssen.
