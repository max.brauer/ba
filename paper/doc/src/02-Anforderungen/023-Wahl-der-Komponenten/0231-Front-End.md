### Front-End (UI)

Für die Oberfläche wurde eine Web-Oberfläche gewählt. Diese bietet den Vorteil, dass ohne viel
Änderungen an der Codebasis die Oberfläche auf verschiedenen Geräten und Systemen angewandt werden
kann. Außerdem modularisiert eine Web-Oberfläche die Codebasis mehr, was die Austauschbarkeit und
Wartung verbessert. Zudem lässt sich dadurch leichter für verschiedene Plattformen entwickeln
(Windows, Linux, Mac, Android, ...).

Im Browser, der die Web-Oberfläche darstellt, gibt es derzeit zwei Technologien, die dafür genutzt
werden können, um Code auszuführen: Javascript und Web-Assembly (kurz: WASM). Javascript ist eine
Skriptsprache, welche früher vom Browser interpretiert wurde (derzeit wird sie meistens vor der
Ausführung übersetzt) und hat den vollen Umfang der Browser APIs. WASM ist eine neuere Technologie,
welche als Byte Code von einer virtuellen Maschine des Browsers ausgeführt wird. WASM hat nur Zugang
zu den meisten Browser APIs, indem es über eine Javascript-Schnittstelle kommuniziert.

\begin{mytable}{Übersicht Programmiersprachen}{uebersicht-ui-programmiersprachen} \centering
\begin{tabulary}{1.0\textwidth}{|*{8}{L|}}
    \hline
    Sprache & C\# & Java & C/C++ & Rust & Java\-Script & Elm & Type\-Script \\
    \hline
    \hline
    Erscheinungs\-jahr & 2001 & 1995 & 1985 & 2015 & 1995 & 2012 & 2012 \\
    \hline
    Browser & WASM & WASM & WASM & WASM & nativ & JS & JS \\
    \hline
\end{tabulary}
\begin{footnotesize}
    Bei Browser wird aufgelistet, welche Technologie genutzt wird, um den Code im Browser
    auszuführen. WASM steht für Web-Assembly und JS für Javascript. Nur Javascript selbst kann direkt
    im Browser ausgeführt werden und muss nicht erst in eine andere Sprache übersetzt werden.
\end{footnotesize}
\end{mytable}

Für die Gestaltung der Web-Oberfläche gibt es eine Vielzahl an Werkzeugen und Systemen (siehe
Tabelle \ref{uebersicht-ui-programmiersprachen}), die alle ihre Vor- und Nachteile haben und sich
mehr oder weniger für dieses Projekt eignen. Bei der Vorauswahl wurde mit Absicht nur eine Teilmenge
der Möglichkeiten herausgesucht, da es den Rahmen dieser Arbeit sonst sprengen würde, wenn alle
berücksichtigt werden. In dem Vergleich wurden in erster Linie Programmiersprachen herausgesucht, mit
denen der Autor vertraut ist oder sich relativ schnell aneignen kann. In diesem Vergleich kommen C#,
Java, C/C++, Rust, Javascript, Type-Script und Elm zum Einsatz.

C# ist eine objektorientierte Sprache, welche 2001 von Microsoft veröffentlicht wurde. Über die
Umgebung Blazor ist es möglich, diese als WASM im Browser auszuführen. Dazu gibt es Nuget, einen
Paketmanager für Dotnet (dazu gehört auch C#) Bibliotheken, welcher es ermöglicht, einfach eine
Vielzahl bereits vorhandener Bibliotheken einzubinden. Dies erleichtert die Entwicklung deutlich,
da so eine Fokussierung auf die individuellen Projektanforderungen möglich ist.

Java ist 1995 von Sun Microsystems veröffentlicht wurden. Früher wurde dies gern für Java Applets im
Browser genutzt, seit der Einführung von HTML 5 wird es aber mehr und mehr von Browsern nicht mehr
unterstützt. Einen großen Einfluss darauf hatte auch, dass dies generell nicht auf Mobilgeräten
genutzt werden konnte. Mittlerweile lässt sich dies über extra Build-Prozesse in WASM übersetzen und
im Browser ausführen.

C/C++ ist die älteste Programmiersprache, welche in dieser Auswahl einbezogen wurde, und wurde 1972
(C) bzw. 1985 (C++) veröffentlicht. Diese Programmiersprache ist sehr hardwarenah und ist quasi das
Schweizer Taschenmesser für alle möglichen Zwecke und Umgebungen. Über einen speziellen Compiler
lässt sich dies in WASM übersetzen.

Rust ist 2015 von Mozilla veröffentlicht wurden und soll genauso wie C/C++ hardwarenahen Code
erlauben, aber auch wie C# oder Java sehr abstrakt sein können. Rust arbeitet sehr effizient mit dem
Arbeitsspeicher und das ohne Garbage Collection oder einer Runtime (wie in C# oder Java). In Rust
geschriebene Programme sind sehr speichersicher, da der Compiler viele Fehler entdeckt und prüft
oder es durch die Programmiersprache nicht möglich ist, diese zu machen. Zu solchen Fehlern zählen
noch benutzten Speicher freizugeben oder auf die gleiche Speicherstelle von unterschiedlichen
Stellen gleichzeitig schreibend zuzugreifen. Ein großes Argument für Rust ist also die Nähe am
Arbeitsspeicher und die hohe Abstraktion ohne zusätzliche Laufzeit, die zur Verfügung gestellt
wird. Zudem kann der Compiler direkt nach WASM übersetzen.

Javascript (1995) ist eine Script-Sprache, welche für Anwendungen im Browser entwickelt wurde. Diese
ist dynamisch typisiert und prototypisch. Dies führt leicht zu schwer erkenn- und wartbaren Fehlern,
da oftmals der genaue Typ und die Daten erst bei der Ausführung bekannt sind. Zudem lassen sich die
Prototypen zur Laufzeit jederzeit bearbeiten, was zu ungewünschten Seiteneffekten führen kann.

Type-Script wurde 2012 von Microsoft als Erweiterung zu Javascript herausgebracht, in der versucht
wurde, ein paar Probleme von Javascript anzugehen. Zum Beispiel ist es jetzt möglich, Typen zu
annotieren, welche von einem Compiler geprüft werden. Es ist aber erlaubt, Type-Script und
Javascript beliebig zu mischen, wodurch es sehr leicht ist, die oben genannten Fehler einzubauen
oder zu verursachen. Für Javascript und Type-Script gibt es eine große Anzahl Frameworks, die die
Entwicklung vereinfachen sollen, aber auf die hier nicht weiter eingegangen wird.

Elm wurde im gleichen Jahr wie Type-Script veröffentlicht, ist funktional und an die
Programmiersprache Haskell angelehnt. Elm wurde für den Browser entworfen, wodurch der Compiler
direkt in Javascript übersetzt. Es gibt derzeit Anstrengungen auch nach WASM zu übersetzen, was aber
derzeit noch nicht vollständig unterstützt wird (siehe \cite{elm-wasm}). Elm hatte sich auch das
Ziel gesetzt, die oben genannten Probleme in Javascript anzugehen. Zum Beispiel schließt dazu der
Elm Compiler Laufzeitfehler generell aus und die Sprache ist typsicher. Weiterhin übersetzt der
Compiler den Quellcode in optimierten Javascript-Code, welcher möglichst kompakt und schnell
ausgeführt werden kann (siehe \cite{elm-speed}).

Diese Technologien sind alle sehr unterschiedlich weit verbreitet, was unter anderem auch daran
liegt, wer diese gefördert hatte. Hinter C# und Type-Script zum Beispiel steht Microsoft und
unterstützt seit Langem die Verbreitung. Dies zeigt sich zum Beispiel an der Menge an verfügbaren
Bibliotheken, Dokumentation und existierenden Projekten.

Hinter Rust stehen primär Mozilla und die Rust Foundation und es gewinnt in den letzten Jahren immer
mehr an Popularität (siehe \cite{rust-popular}). Es wird vor allem die hohe Performance gelobt, hat
aber auf der anderen Seite eine äußerst steile Lernkurve.

Es ist zwar, wie oben gesagt, möglich Java und C/C++ im Browser auszuführen, aber dies ist eher
weniger dokumentiert und es gibt vergleichsweise wenige Projekte dazu.

Im Gegensatz dazu steht Elm, was sehr gut dokumentiert ist, eine große Bibliothek hat und recht
leicht zu erlernen ist. Die Verbreitung dahinter ist vergleichsweise eher gering einzuschätzen.

Für diese Arbeit wurden folgende Kriterien für die Auswahl der Programmiersprache festgelegt:

1. Sie muss im Browser ausführbar sein.
2. Sie muss sicher und möglichst ohne Laufzeitfehler funktionieren.
3. Sie muss leicht zu debuggen und zu warten sein.
4. Sie muss eine gute Dokumentation existieren.

Zumindest Punkt 1 und 4 lässt sich bei allen mit "Ja" beantworten. Punkt 2 lässt sich aus
Erfahrungssicht des Autors nur bei Rust und Elm mit "Ja" beantworten. Es ist aber möglich, bei den
anderen Programmiersprachen dies mit mehr oder weniger Aufwand zu erreichen.

Bei Punkt 3 muss hier eine Unterscheidung getroffen werden. Das Problem ist hierbei WASM, was nur
über eine virtuelle Maschine im Browser ausgeführt wird und daher keinen direkten Zugang hat.
Hierfür muss erst eine spezielle Umgebung eingerichtet werden, was je nach Programmiersprache mehr
oder weniger aufwendig ist. Klar im Vorteil ist hier Javascript, da die meisten modernen Browser
genügend Werkzeuge mitliefern.

Ein Ausreißer ist hierbei Elm, da es von einer anderen Sprache in Javascript übersetzt wird, sind
die Werkzeuge des Browsers teilweise nicht hilfreich (zumindest die, auf die der Code genauer
eingehen) oder nutzlos (da das Konzept von Elm diese nicht braucht). Dafür ist Elm stark
modularisiert und funktional aufgebaut und bietet für sein Modell-Update-View-Konzept genügend
eigene Werkzeuge, um zu debuggen.

Insgesamt wurde sich hier für Elm entschieden, da es zum einen alle Kriterien erfüllen kann und zum
anderen der Entwicklungsprozess damit vergleichsweise leicht und schnell vonstattengehen kann.
