## Datenschutz

Datenschutz ist in der europäischen Datenschutzgrundverordnung DSGVO (GDPR), den
Landesdatenschutzgesetzen der einzelnen Bundesländer und den Datenschutzgesetzen der Staaten, in dem
die betroffenen Bürger wohnen, gesetzlich geregelt. Der gesetzliche Umfang sprengt den Umfang dieser
Arbeit bei Weitem, wodurch nur kurz auf die gesetzliche Lage zum Schutz vor Fremden und der
Betroffenen genauer eingegangen wird.

Auf viele dieser Regelungen wird im Kapitel \ref{030-sicherheit} nur grob eingegangen, da diese
zudem Handlungsempfehlungen des BSI oder Grundsätze der IT haben.
