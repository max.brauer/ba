### Schutz vor Fremdzugriff

Die Daten befinden sich auf physischen Geräten (Laptop, Festplatte, USB-Stick, ...), welche mit dem
Forscher weltweit mitgenommen werden. Dabei kann es sein, dass die Datenträger in fremde Hände
gelangen können. Um hierbei die Daten selbst zu schützen, ist es zwingend erforderlich, die Daten so
zu schützen, dass sie selbst mit vertretbarem Aufwand nicht les- oder manipulierbar sind.

In erster Linie hierzu werden hierzu die gespeicherten Dateien verschlüsselt und sind somit ohne
ihren Schlüssel nicht mehr auszulesen. Zusätzlich wird noch ein Hashwert über die verschlüsselte
Datei ermittelt, welcher es ermöglicht, Manipulationen an der Datei zu entdecken, aber nicht zu
beheben.

Die Namen, Pfade, Schlüssel und Hashwerte der Dateien sind alles Metadaten, welche alle in einer
Datenbank hinterlegt werden. Mithilfe dieser Informationen erhält die Anwendung (oder auch der
Angreifer) Zugriff auf alle Daten, die in den Dateien gespeichert wurden. Ohne diese ist auch
gleichzeitig kein Zugriff möglich. Damit die Datenbank selbst wiederum geschützt ist, wird diese
verschlüsselt. Hierfür gibt es verschiedene Möglichkeiten (siehe \ref{0233-datenbank}). Damit
existiert nur noch eine Stelle, wo ein Schlüssel notwendig ist, welcher alles andere freischaltet.

Zusätzlich zur Verschlüsselung der Dateien und der Datenbank ist es auch möglich, die komplette
Festplatte (bzw. Partition) inklusive der Daten da drauf zu verschlüsseln. Dies geht unter Linux mit
Luks und unter Windows mit BitLocker sehr gut. Das hat den Vorteil, dass ohne den Schlüssel für die
Festplatte einen Zugriff auf die Datenbank oder Dateien nicht mehr möglich ist. Sobald die
Festplatte aber geöffnet ist (Schlüssel wurde Luks, BitLocker, etc. übermittelt), dann sind alle
Dateien darauf allen Prozessen vom Rechner transparent verfügbar, so als wären die nicht
verschlüsselt gewesen.

Eine Verschlüsselung der Festplatte schützt somit nur vor Fremden, die von außen versuchen, auf das
Gerät zuzugreifen. Andere Prozesse auf dem gleichen Gerät hätten nach wie vor Zugriff auf alle
Daten, wenn diese nicht noch zusätzlich verschlüsselt wären.

Ein weiteres Problem stellt der verwendete Schlüssel dar, der für die Ent- und Verschlüsselung der
Festplatte, der Dateien und der Datenbank genutzt wird. Wird immer wieder der gleiche Schlüssel
verwendet, wird somit Fremden es erleichtert, diese einmal auszulesen und in Zukunft erneut für
gleiche oder andere Datensätze einzugeben. Dies wird in der IT auch als Replay-Angriff bezeichnet.
Eine Möglichkeit, dies zu umgehen, stellen Einmal-Passwörter (engl. One-Time-Pad) dar, bei der für
jede Ent- und Verschlüsselung ein neuer, bisher nicht verwendeter Schlüssel genutzt wird. In der
Regel wird versucht, dass aus der Historie bisheriger Schlüssel der nächste Schlüssel nicht
abgeleitet werden kann.

Und schließlich stellt sich für die Anwendung die Frage, wann diese den Schlüssel erhält, um die
benötigten Daten zu entschlüsseln. Dies könnte direkt beim Start der Anwendung über ein Argument
oder Konfigurationsdatei geschehen. Das hätte aber zur Folge, dass für den kompletten Lebenszyklus
der Anwendung der Schlüssel bekannt wäre. Bei einer Konfigurationsdatei sogar über diesen hinaus.
Bei der Übergabe als Argument können sogar alle anderen Prozesse auf dem Rechner den Schlüssel
sehen, wenn diese kurz in die Prozesstabelle reinschauen. Alternativ könnte die Anwendung zu einem
beliebigen Zeitpunkt den Schlüssel erhalten und diesen auch nur für die einzelne Aufgabe oder
Sitzung des Nutzers verwenden. Das hätte den Vorteil, dass der Schlüssel möglichst kurz bekannt ist
und die Anwendung dennoch Zugriff auf die Daten erhält, wenn diese gerade benötigt werden.

Eine weitere wichtige Möglichkeit zum Schutz vor Fremdzugriff ist eine 2-Faktor-Authentifizierung.
Hiermit wird zusätzlich zu einem Geheimnis, was in diesem Fall der Schlüssel für die Entschlüsselung
sein kann, ein weiterer Faktor, z. B. Besitz abgefragt. Nur wenn der Nutzer den zweiten Faktor
bereitstellt und dieser verifiziert werden kann, wird eine Anmeldung als erfolgreich angesehen und
dem Nutzer die Daten bereitgestellt. Für den zweiten Faktor gibt es derzeit eine Fülle an
Möglichkeiten. Ein paar Ausgewählte sind:

- Versand eines kurzlebigen Einmal-Codes per SMS an eine bekannte Nummer. Wird derzeit noch von
  PayPal, Google und früher für Online-Banking genutzt.
- Vorher generierte Liste an Einmal-Codes, welche ausgedruckt vorliegt. Wurde früher gern für
  Online-Banking genutzt. Die Martin-Luther-Universität Halle-Wittenberg nutzt dies noch heute für
  ihr Selbstverwaltungsportal.
- Versand eines kurzlebigen Einmal-Codes per E-Mail an eine bekannte Adresse. Wird derzeit von vielen
  System gern verwendet (u.A. Microsoft, Steam).
- Bereitstellen von kurzlebigen Einmal-Codes per Handy-App. Das bekannteste und verbreitetste hierzu
  ist der Google Authenticator, bei der beliebige Dienstleister (z. B. GitHub, Nintendo, Rockstar,
  Ubisoft, Discord, NVIDIA, ...) ihre Codes abfragen können. Viele Banken (z. B. Commerzbank,
  Postbank und Sparkasse) stellen hierzu eigene Apps bereit.
- Nutzung von Hardware-Tokens, die kryptografische Schlüssel austauschen und somit ihre Identität
  verifizieren. Dies gibt es in verschiedenen Formen, unter anderem SIM-Karten (z. B. Autorisierung
  gegenüber dem Provider), elektronischen Ausweisen (z. B. Personalausweis) und USB-Sticks (z. B.
  FIDO).

Generell gilt, jeder zweite Faktor ist besser als gar kein zweiter Faktor. Manche brauchen mehr
Aufwand in der Bereitstellung, da extra Server und Dienstleister kontaktiert werden müssen und
andere können direkt zwischen Nutzer und Anwendung geregelt werden. Außerdem bieten alle auch eine
unterschiedliche Sicherheit anderen gegenüber. Bei E-Mails wird sich auf die Sicherheit des
Übertragungsweges der Server und des Postfachs des Empfängers verlassen. Bei Handy-Apps auf den
jeweiligen Dienstleister. Und bei Hardwaretokens vor Manipulation der Hardware (was mit erhöhten
Aufwand verbunden ist).

Für diese Anwendung wird als zweiter Faktor eine FIDO USB Key genutzt (siehe Kapitel
\ref{034-fido}). Dies hat den Vorteil, dass dies lokal am gleichen Rechner funktioniert, eine hohe
Sicherheit hat und leicht zu benutzen ist.
