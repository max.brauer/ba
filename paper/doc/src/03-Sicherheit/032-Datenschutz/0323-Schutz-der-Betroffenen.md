### Schutz der Betroffenen

In der Datenbank können Patienteninformationen oder persönliche Meinungen und Weltanschauungen
vorhanden sein, welche nicht nachträglich mit dem Interviewten wieder verknüpft werden dürfen.
Selbst für den Forscher dürfen diese Zusammenhänge nicht mehr nachträglich (alleinig über die
Datenbank) gezogen werden. Dadurch wird auch das Datenbankschema maßgeblich beeinflusst.

Des Weiteren gibt es in den Daten mit erhöhtem Schutzbedarf, da diese Rückschlüsse auf die Identität
der Person ermöglichen und somit der Schutz der Betroffenen nicht gewährleistet werden kann. Diese
müssen anonymisiert werden können. Außerdem hat der Betroffene jederzeit das Recht, seine Daten
löschen zu lassen. Dies gilt auch für anonymisierte Daten.

Von daher ist die Datenbank in zwei Teile aufgeteilt. Die eine enthält alle normalen Daten, welche
für die Forschung relevant sind. Die andere enthält besonders schützenswerte Daten, die nach der
Anonymisierung nicht mehr benötigt werden. Der zweite Teil, auch Confidential Database in dieser
Arbeit genannt, soll zu jeder Zeit entfernt und in speziellen Archiven gesichert werden könne. Für
den Forscher steht diese dann nicht mehr zur Verfügung. Wünscht der Betroffene eine Löschung, so kann
die Confidential Database wieder zurückgespielt werden und die Daten lassen sich somit auffinden und
löschen.
