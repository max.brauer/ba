## Bildschirmfotos

Hier werden ein paar Bildschirmfotos von der Weboberfläche der Anwendung gezeigt.

\includegraphics[scale=0.36]{scr-home.png}\newline

Dieses Bild zeigt die Startseite der Anwendung. Hier sind schon einige Datenbanken und Schemas
eingetragen. Diese wurden alle nur zu Testzwecken hinzugefügt und haben keine Realdaten.

Hier ist auch die Aufteilung der Oberfläche erkenntlich. Auf der linken Seite ist die
Navigation mit einer Sprungliste der zuletzt besuchten Seiten. Im rechten Hauptteil ist je nach
Oberfläche der Hauptinhalt zu sehen.

\includegraphics[scale=0.38]{scr-suche.png}\newline

Hier wird eine Suche gezeigt, bei der noch die Ergebnisse ermittelt werden.

\includegraphics[scale=0.38]{scr-interview.png}\newline

Dies zeigt eine Ansicht eines Interviews.

\includegraphics[scale=0.38]{scr-add-person.png}\newline

Hier wird eine neue Person zu einem Interview hinzugefügt. Dabei wird eine Liste der Rollen
angezeigt, welche für die Datenbank zur Verfügung stehen.

\includegraphics[scale=0.38]{scr-person.png}\newline

Hier wird eine Person mit ein paar Eingaben zu dieser gezeigt.

\includegraphics[scale=0.38]{scr-files.png}\newline

Dies ist eine Liste von Dateien, die mit der einen Person verknüpft sind.

\includegraphics[scale=0.38]{scr-access.png}\newline

Das Bild zeigt die Zugriffssteuerung einer Datenbank. Hier können Zugänge für eine Datenbank
hinterlegt, bearbeitet oder gelöscht werden. Außerdem lässt sich hier auch der Zugang zu einer
Datenbank mit einem Master Key wiederherstellen. 

\includegraphics[scale=0.38]{scr-login.png}\newline

Das Bild zeigt die Anmeldung. In diesem Fall soll eine bestimmte Datenbank freigeschaltet werden.
