# Datenbankmanagementsysteme

| Kategorie | [MariaDB](https://mariadb.org/) | [MySQL](https://www.mysql.com/de/) | [SQLite](https://sqlite.org/index.html) | [MongoDB](https://www.mongodb.com/de-de) | [LiteDB](https://www.litedb.org/docs/) |
|-|-|-|-|-|-|
| Relational | x | x | x | | |
| Standalone | x | x | | x | |
| Lizenz | GPL 2.0 | GPL 2.0 (*1) | public domain | [SSPL v1](https://github.com/mongodb/mongo/blob/master/LICENSE-Community.txt) | MIT |
| Open Source | [GitHub](https://github.com/MariaDB/) | [GitHub](https://github.com/mysql/mysql-server) | [Sqlite.org (Fossil)](https://sqlite.org/src/dir?ci=trunk) | [GitHub](https://github.com/mongodb/mongo) | [GitHub](https://github.com/mbdavid/litedb) |
| Preis | free | free (*1) | free | free | free |
||
| **lokale Verschlüsselung** |
| komplette Datenbank | [supported](https://mariadb.com/kb/en/data-at-rest-encryption-overview/) | [nur in kostenpflichtiger Enterprise-Version](https://www.mysql.com/products/enterprise/tde.html) | [alternative](https://github.com/praeclarum/sqlite-net) | [aufwändig und nur Felder](https://www.mongodb.com/basics/mongodb-encryption) | [supported](https://www.litedb.org/docs/encryption/) |
| einzelne Tabellen | supported | | nur komplette Datenbank | nur Felder | nur komplette Datenbank |
| Engines | XtraDB, InnoDB, teilw. Aria || alle | alle | alle |
| Speicher der Schlüssel | Datei | zentral in einer Schlüsselverwaltung | frei | frei | frei |
| Overhead | 3-5% | | ? | ? | ? |
| Methode | AES | AES | AES | AES | AES |
||
| **Transportverschlüsselung** |
| Art | [TLS](https://mariadb.com/docs/security/encryption/in-transit/) | [TLS](https://dev.mysql.com/doc/refman/5.7/en/encrypted-connections.html) ([bessere doc](https://linuxhint.com/mysql-encryption-transit/)) | none (in process) | [TLS](https://www.mongodb.com/basics/mongodb-encryption) | none (in process)
||
| **Authorisierung** |
| Nutzerauthorisierung | x | x | | x | |
| Mehre Accounts | x | x | | x | |
| Rollen | x | x | | x | |
| Rechtemanagement | detailiert | detailiert | | detailiert | |
||
| **Weiteres** |
| Erster Release | [29.10.2009](https://en.wikipedia.org/wiki/MariaDB) | [23.05.1995](https://en.wikipedia.org/wiki/MySQL) | [17.08.2000](https://en.wikipedia.org/wiki/SQLite) | [11.02.2009](https://en.wikipedia.org/wiki/MongoDB) | [17.09.2016](https://github.com/mbdavid/LiteDB/commit/4f3cd3a8c12ef65a3d3ddf96664928f9a1b83789) |
| Roadmap für die nächsten 5 Jahre | [Pläne für nächsten Release](https://mariadb.com/kb/en/mariadb-roadmap/) | - | [Nächste Version](https://sqlite.org/src4/doc/trunk/www/design.wiki) | - | - |
| Herkunft (Neuentwicklung, Fork) | MySQL Fork | Neuentwicklung in Schweden, mehrfach verkauft | Neuentwicklung beim US Militär, Syntax an PostgreSQL angelehnt | Neuentwicklung bei 10gen | Neuentwicklung |

(*1): nur kostenlos, wenn alles unter GPL 2.0 veröffentlich wird. Dann ist MySQL auch unter GPL 2.0 verfügbar.

## lokale Verschlüsselung unter MariaDB

- eventuelle Backupprobleme, externe Programme können verschlüsselte Logs nicht lesen (mit Ausnahme von MariaDB Backup)
- Galera gcache ist unverschlüsselt in Community Version. In Enterprise Server ist es dennoch verschlüsselt. (Wird für Cluster benötigt)
- Dateibasierte `general query log` und `slow query log` sind unverschlüsselt
- `aria log` ist unverschlüsselt
- `error log` ist unverschlüsselt
- viele Konfigurationen müssen angepasst werden
- es muss ein Verschlüsselungsplugin installiert werden
- Verschlüsselungskeys liegen auf der Platte als Konfigurationsdatei
    - Die Keys selber können verschlüsselt werden, dafür muss der Schlüssel wieder als Datei vorliegen
    - [Quelle](https://mariadb.com/kb/en/file-key-management-encryption-plugin/)

## Quellen

- [MySQL Lizenzmodell](https://www.mysql.com/about/legal/licensing/oem/)
