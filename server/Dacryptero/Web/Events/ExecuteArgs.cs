using Dacryptero.Notifications;

namespace Dacryptero.Web.Events
{
    public class ExecuteArgs : ITransmitter
    {
        public WebSocketConnection Connection { get; }

        public DB.IDBDataQuery Data => Connection.Session;

        bool ITransmitter.HasLocalNotification => true;

        bool ITransmitter.HasIdMapped => false;

        public ExecuteArgs(WebSocketConnection connection)
        {
            Connection = connection;
        }

        public void SendNotification(Notification notification)
        {
            _ = Connection.Send(new Send.SendNotification(notification));
        }

        public void SendGlobalNotification(Notification notification)
        {
            _ = WebSocketConnection.Foreach(x => x.Send(new Send.SendNotification(notification)));
        }
    }
}