namespace Dacryptero.Web.Events.Send
{
    public enum EditTarget
    {
        Interview,
        Person,
        Attribute
    }
}