using System.Text.Json;

namespace Dacryptero.Web.Events.Send;

public class AccountFidoRequest : SendBase
{
    public string Command { get; }

    public string? DatabaseName { get; }

    public JsonElement Value { get; }

    public AccountFidoRequest(string command, string? databaseName, JsonElement value)
    {
        Command = command;
        DatabaseName = databaseName;
        Value = value;
    }

    protected override void WriteJsonContent(Utf8JsonWriter writer)
    {
        writer.WriteString("command", Command);
        writer.WriteString("database", DatabaseName);
        writer.WritePropertyName("value");
        Value.WriteTo(writer);
    }
}