using System.Text.Json;
using Dacryptero.Notifications;

namespace Dacryptero.Web.Events.Send
{
    public class SendNotification : SendBase
    {
        public Notification Notification { get; }

        public SendNotification(Notification notification)
        {
            Notification = notification;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("id", Notification.Id);
            writer.WriteString("img", Notification.Image);
            writer.WriteString("title", Notification.Title);
            writer.WriteString("description", Notification.Description);
            writer.WriteStartArray("progress");
            if (Notification.HasProgress)
            {
                if (Notification.Progress.HasValue)
                {
                    writer.WriteStringValue("progress");
                    writer.WriteNumberValue(Notification.Progress.Value);
                }
                else writer.WriteStringValue("undefined");
            }
            else writer.WriteStringValue("none");
            writer.WriteEndArray();
            if (Notification.Close is null)
                writer.WriteNull("close");
            else writer.WriteString("close", Notification.Close.Value);
        }
    }
}