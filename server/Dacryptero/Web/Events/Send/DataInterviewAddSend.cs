using System.Text.Json;
using LiteDB;

namespace Dacryptero.Web.Events.Send
{
    public class DataInterviewAddSend : SendBase
    {
        public ObjectId Id { get; }

        public DataInterviewAddSend(ObjectId id)
        {
            Id = id;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("id", Id.ToString());
        }
    }
}