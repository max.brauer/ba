using System.Text.Json;

namespace Dacryptero.Web.Events.Send
{
    public class EditDenied : EditAccept
    {
        public EditDeniedReason ReasonCode { get; }

        public string Reason { get; }

        public EditDenied(string id, string? key, EditTarget target, EditDeniedReason reasonCode, string reason)
            : base(id, key, target)
        {
            ReasonCode = reasonCode;
            Reason = reason;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            base.WriteJsonContent(writer);
            writer.WriteString("reasonCode", ReasonCode.ToString());
            writer.WriteString("reason", Reason);
        }
    }
}