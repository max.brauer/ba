using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace Dacryptero.Web.Events.Receive
{
    public class FileChangeConfidential : ReceiveBase
    {
        public string Id { get; private set; } = "";

        public bool Confidential { get; private set; }

        public FileChangeConfidential() {}

        public FileChangeConfidential(string id, bool confidential)
        {
            Id = id;
            Confidential = confidential;
        }

        public override async Task Execute(ExecuteArgs args)
        {
            var id = Id.ToObjectId();
            if (id is null)
                return;
            
            var file = await args.Data.GetFileAsync(id, CancellationToken.None)
                .ConfigureAwait(false);
            if (file is null)
            {
                args.SendNotification(Notifications.Notification.Error(
                    "File not found",
                    ""
                ));
                return;
            }
            
            var result = await file.Value.Source
                .UpdateFileConfidential(file.Value.Value, Confidential, CancellationToken.None)
                .ConfigureAwait(false);

            if (result is null || result.Value.Value.Count == 0)
            {
                args.SendNotification(Notifications.Notification.Error(
                    "Cannot update " + (file.Value.Value.Mime == "folder" ? "Folder" : "File"),
                    file.Value.Value.Name
                ));
                return;
            }
            
            if (result.Value.Value.Count == 1)
                args.SendGlobalNotification(
                    new Notifications.Notification
                    {
                        Close = DateTime.UtcNow.AddSeconds(15),
                        Title = (file.Value.Value.Mime == "folder" ? "Folder" : "File")
                            + " marked as "
                            + (Confidential ? "confidential" : "non-confidential"),
                        Description = file.Value.Value.Name,
                        Image = file.Value.Value.Mime == "folder" 
                            ? "/img/svgrepo/interaction-set/folder-53011.svg" 
                            : "/img/svgrepo/interaction-set/file-30366.svg"
                    }
                );
            else
            {
                args.SendGlobalNotification(
                    new Notifications.Notification
                    {
                        Close = DateTime.UtcNow.AddSeconds(15),
                        Title = result.Value.Value.Count.ToString() 
                            + " entries marked as "
                            + (Confidential ? "confidential" : "non-confidential"),
                        Image = "/img/svgrepo/essential-set-2/repeat-84008.svg"
                    }
                );
            }
            await WebSocketConnection.Foreach(async con =>
            {
                await Task.WhenAll(
                    result.Value.Value.Select(
                        entry => con.Send(new Events.Send.FileUpdated(
                            new DB.DBValue<Data.File>(result.Value.Source, entry)
                        ))
                    )
                ).ConfigureAwait(false);
            }).ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Id = json.GetProperty("id").GetString() ?? "";
            Confidential = json.GetProperty("confidential").GetBoolean();
        }
    }
}