using System.Text.Json;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive
{
    public class SearchSend : ReceiveBase
    {
        public string Query { get; private set; } = "";

        public string? UiId { get; private set; }

        public override async Task Execute(ExecuteArgs args)
        {
            await Search.SearchTask.SearchAsync(args.Connection, UiId, Query)
                .ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Query = json.GetProperty("query").GetString() ?? "";
            if (json.TryGetProperty("uiId", out JsonElement node))
                UiId = node.GetString();
        }
    }
}