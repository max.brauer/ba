using System.Text.Json;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive
{
    public class SchemaRequest : ReceiveBase
    {
        public string Name { get; private set; } = "";

        public override async Task Execute(ExecuteArgs args)
        {
            var schema = Data.Schema.SchemaCollection.Schemas
                .TryGetValue(Name, out Data.Schema.SchemaFile? s) ? s :
                new Data.Schema.SchemaFile("", Name, null);
            await args.Connection.Send(new Send.SchemaResponse(Name, schema))
                .ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Name = json.GetProperty("name").GetString() ?? "";
        }
    }
}