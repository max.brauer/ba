using System.Threading.Tasks;
using MaxLib.WebServer;
using System.IO;

namespace Dacryptero.Web
{
    public class RootService : WebService
    {
        public RootService() 
            : base(ServerStage.CreateDocument)
        {
        }

        public override bool CanWorkWith(WebProgressTask task)
            => task.Request.Location.DocumentPathTiles.Length == 0
                && File.Exists("ui/index.html");

        public override Task ProgressTask(WebProgressTask task)
        {
            task.Document.DataSources.Add(new HttpFileDataSource(Path.Combine(Config.Default.Server.UiDir, "index.html"))
            {
                MimeType = MimeType.TextHtml,
            });
            return Task.CompletedTask;
        }
    }
}