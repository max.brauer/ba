namespace Dacryptero.Notifications
{
    public class GlobalTransmitter : ITransmitter
    {
        public bool HasGlobalFallback { get; }

        public bool HasLocalNotification => false;

        public bool HasIdMapped => false;

        /// <summary>
        /// Creates a new <see cref="ITransmitter" /> that can only send global messages and
        /// therefore independed from direct connection.
        /// </summary>
        /// <param name="globalFallback">
        /// if true all local notifications will be send globaly instead. If false all these
        /// notifications will be discarded instead
        /// </param>
        public GlobalTransmitter(bool globalFallback = false)
        {
            HasGlobalFallback = globalFallback;
        }

        public void SendGlobalNotification(Notification notification)
        {
            _ = Web.WebSocketConnection.Foreach(x => x.Send(
                new Web.Events.Send.SendNotification(notification)
            ));
        }

        public void SendNotification(Notification notification)
        {
            if (HasGlobalFallback)
                SendGlobalNotification(notification);
        }
    }
}