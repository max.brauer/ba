using System;

namespace Dacryptero.Notifications
{
    public interface ITransmitter
    {
        bool HasLocalNotification { get; }

        bool HasIdMapped { get; }

        void SendNotification(Notification notification);

        void SendGlobalNotification(Notification notification);
    }
}