using System;

namespace Dacryptero.Notifications
{
    /// <summary>
    /// This class will map all <see cref="Notification" /> that has an empty id (not null) to the
    /// id that is specified in <see cref="Id" />.
    /// </summary>
    public class IdMapper : ITransmitter
    {
        public ITransmitter Transmitter { get; }

        public string Id { get; }

        public bool HasLocalNotification => Transmitter.HasLocalNotification;

        public bool HasIdMapped => true;

        public IdMapper(ITransmitter transmitter, string id)
        {
            Transmitter = transmitter;
            Id = id;
        }

        public void SendNotification(Notification notification)
        {
            if (notification.Id == "")
                notification.Id = Id;
            Transmitter.SendNotification(notification);
        }

        public void SendGlobalNotification(Notification notification)
        {
            if (notification.Id == "")
                notification.Id = Id;
            Transmitter.SendGlobalNotification(notification);
        }
    }
}