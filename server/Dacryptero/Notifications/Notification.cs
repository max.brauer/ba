using System;

namespace Dacryptero.Notifications
{
    public class Notification
    {
        public string? Id { get; set; }

        public string? Image { get; set; }

        public string Title { get; set; } = "";

        public string Description { get; set; } = "";

        public bool HasProgress { get; set; }

        public double? Progress { get; set; }

        public DateTime? Close { get; set; }

        public static Notification Error(string title, string description)
        {
            return new Notification
            {
                Image = "/img/svgrepo/essential-set-2/error-138890.svg",
                Title = title,
                Description = description,
                Close = DateTime.UtcNow.AddSeconds(30),
            };
        }

        public static Notification TfaWarning(string method)
        {
            return new Notification
            {
                Image = "/img/svgrepo/essential-set-2/target-36339.svg",
                Title = $"[UNSECURE] {method} finished",
                Description = "It is recommended to switch on TFA in the configuration",
                Close = DateTime.UtcNow.AddSeconds(30),
            };
        }
    }
}