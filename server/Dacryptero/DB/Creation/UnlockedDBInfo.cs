using System;

namespace Dacryptero.DB.Creation;

/// <summary>
/// This class holds the information about the current unlocked database. This class should be
/// removed from the RAM as fast as possible because it contains the decryption key for the
/// database.
/// </summary>
public class UnlockedDBInfo
{
    public DBSingleController DB { get; }

    public ReadOnlyMemory<byte> Key { get; }

    public DateTime Opened { get; }

    public UnlockedDBInfo(DBSingleController db, ReadOnlyMemory<byte> key)
    {
        DB = db;
        Key = key;
        Opened = DateTime.UtcNow;
    }
}