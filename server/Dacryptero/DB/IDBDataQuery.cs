using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using LiteDB;

namespace Dacryptero.DB
{
    public interface IDBDataQuery
    {
        Task<DBValue<Data.Person>?> GetPersonAsync(ObjectId id, CancellationToken cancellatonToken);

        Task<DBValue<bool>?> UpdatePersonAsync(Data.Person person, CancellationToken cancellationToken);

        Task<DBValue<Data.File>?> GetFileAsync(ObjectId id, CancellationToken cancellationToken);

        Task<DBValue<Data.FileEntry>?> GetFileEntryAsync(ObjectId id, CancellationToken cancellationToken);

        IAsyncEnumerable<DBValue<Data.File>> EnumerateFilesAsync(ObjectId person, ObjectId? root, CancellationToken cancellationToken);

        Task<DBValue<bool>?> UpdateFile(Data.File file, CancellationToken cancellationToken);

        Task<DBValue<Data.Interview>?> GetInterviewAsync(ObjectId id, CancellationToken cancellationToken);

        Task<DBValue<bool>?> UpdateInterviewAsync(Data.Interview interview, CancellationToken cancellationToken);

        IAsyncEnumerable<DBValue<Data.Person>> EnumerateRawPeopleFromInterviewAsync(ObjectId interview, CancellationToken cancellationToken);

        IAsyncEnumerable<Search.SearchResult> DoSearch(Search.AST.AstBase ast, 
            Search.Sorting.SortConfig sortConfig, CancellationToken cancellationToken
        );
    }
}