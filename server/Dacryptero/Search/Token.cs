using System;

namespace Dacryptero.Search
{
    public readonly struct Token
    {
        public Range Range { get; }

        public TokenType Type { get; }

        public Token(Range range, TokenType type)
        {
            Range = range;
            Type = type;
        }

        public override bool Equals(object? obj)
        {
            return obj is Token token &&
                   Range.Equals(token.Range) &&
                   Type == token.Type;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Range, Type);
        }

        public static bool operator ==(Token left, Token right)
            => left.Equals(right);

        public static bool operator !=(Token left, Token right)
            => !left.Equals(right);
    }
}