namespace Dacryptero.Search.AST
{
    public enum FieldComparer
    {
        Equality,
        LowerOrEqual,
        Lower,
        HigherOrEqual,
        Higher,
        Contains,
        StartWith,
        EndsWith,
        Regex,
    }
}