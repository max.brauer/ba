using System.Threading;
using System.Threading.Tasks;
using Dacryptero.DB;

namespace Dacryptero.Search.AST
{
    public class Fail : AstBase
    {
        public static Fail Default { get; } = new Fail();

        public override SearchProgress Check(SearchContext context)
        {
            return new SearchProgress();
        }

        public override AstBase Clone()
        {
            return this;
        }

        public override Task<SearchIndex> GetSearchIndexAsync(DBSingleController db,
            Data.IndexSource source, CancellationToken cancellationToken
        )
        {
            return Task.FromResult(SearchIndex.Empty);
        }
    }
}