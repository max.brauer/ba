namespace Dacryptero.Search
{
    /// <summary>
    /// The type of the token in the search string
    /// </summary>
    public enum TokenType
    {
        /// <summary>
        /// A word that is searched for. This can also be multiple word if the original expression
        /// was escaped.
        /// </summary>
        Word,
        /// <summary>
        /// The name of an option.
        /// </summary>
        OptionName,
        /// <summary>
        /// The value of an option.
        /// </summary>
        OptionValue,
        /// <summary>
        /// The following expression is negated
        /// </summary>
        Negation,
        /// <summary>
        /// This is an operator like &amp;&amp; or ||
        /// </summary>
        Operator,
        /// <summary>
        /// A group with an ( is started.
        /// </summary>
        GroupStart,
        /// <summary>
        /// A group with an ) is ended.
        /// </summary>
        GroupEnd,
    }
}