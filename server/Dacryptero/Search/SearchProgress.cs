using System.Collections.Generic;

namespace Dacryptero.Search
{
    public class SearchProgress
    {
        public double Score { get; }

        public bool IsSuccess => Score >= 1e-10;

        public Collections.TreeCollection<ResultFound> Previews { get; }

        public SearchProgress()
        {
            Score = 0;
            Previews = new Collections.TreeCollection<ResultFound>();
        }

        public SearchProgress(double score, Collections.TreeCollection<ResultFound> previews)
        {
            Score = score;
            Previews = previews;
        }

    }
}