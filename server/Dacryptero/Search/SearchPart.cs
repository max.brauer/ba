using LiteDB;

namespace Dacryptero.Search
{
    public class SearchPart
    {
        public FoundType Type { get; }

        public string Name { get; }

        public BsonValue Value { get; }

        public SearchPart(FoundType type, string name, BsonValue value)
        {
            Type = type;
            Name = name;
            Value = value;
        }
    }
}