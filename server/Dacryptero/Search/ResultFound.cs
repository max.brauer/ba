using System.Collections.Generic;
using System.Text.Json;

namespace Dacryptero.Search
{
    public class ResultFound
    {
        public enum FoundPreviewType
        {
            Ellipsis,
            Context,
            Term
        }

        public readonly struct FoundPreview
        {
            public FoundPreviewType Type { get; }

            public string? Value { get; }

            public FoundPreview(FoundPreviewType type, string? value)
            {
                Type = type;
                Value = value;
            }
        }

        public FoundType Type { get; }

        public string Field { get; }

        public List<FoundPreview> Preview { get; }
            = new List<FoundPreview>();
        
        public ResultFound(FoundType type, string field)
        {
            Type = type;
            Field = field;
        }

        public void WriteTo(Utf8JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WriteString("field", Field);
            writer.WriteString("type", Type.ToString());
            writer.WriteStartArray("value");
            foreach (var preview in Preview)
            {
                writer.WriteStartObject();
                writer.WriteString("type", preview.Type.ToString());
                if (preview.Value is not null)
                    writer.WriteString("value", preview.Value);
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
            writer.WriteEndObject();
        }
    }
}