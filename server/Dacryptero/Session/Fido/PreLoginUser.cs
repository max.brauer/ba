using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Dacryptero.Web;
using Fido2NetLib;

namespace Dacryptero.Session.Fido;

public class PreLoginUser : UserBase
{
    public string? Name { get; }

    public List<DB.DBSingleController> Controller { get; }

    public bool UnlockDatabase { get; }

    public PreLoginUser(WebSocketConnection connection, Fido2Configuration configuration,
        string? name, List<DB.DBSingleController> controller, bool unlockDatabase
    )
        : base(connection, configuration)
    {
        Name = name;
        Controller = controller;
        UnlockDatabase = unlockDatabase;
    }

    public override (string method, JsonElement data) GetMessage()
    {
        throw new NotSupportedException("Cannot perform second step before first");
    }

    public override Task<string?> ValidateResult(JsonElement data)
    {
        throw new NotSupportedException("Cannot perform second step before first");
    }
}