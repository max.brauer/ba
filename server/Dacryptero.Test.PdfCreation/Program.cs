﻿using System;
using SelectPdf;

namespace Dacryptero.Test.PdfCreation;

public class Program
{
    public static void Main(string[] args)
    {
        /* yay -S chromium
        */
        var converter = new HtmlToPdf();
        converter.Options.RenderingEngine = RenderingEngine.Blink;
        if (Environment.OSVersion.Platform == PlatformID.Win32NT)
        {
            var engine = GetBlinkPath();
            if (engine != null)        
                converter.Options.BlinkEnginePath = engine + "/chrome.exe";
        }
        if (Environment.OSVersion.Platform == PlatformID.Unix)
        {
            converter.Options.BlinkEnginePath = "/usr/bin/chromium";
        }
        converter.Options.PdfPageSize = PdfPageSize.A4;
        converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
        converter.Options.DrawBackground = true;
        converter.Options.MarginBottom = 30;
        converter.Options.MarginLeft = 30;
        converter.Options.MarginRight = 30;
        converter.Options.MarginTop = 30;
        // var doc = converter.ConvertUrl("https://de.wikipedia.org/wiki/Deutschland");
        var doc = converter.ConvertUrl("http://localhost:8015/security/master-key?session=gNQtmMvr34zEMlP4Fgq8lxRMPfth9IvkJ7up1e%2Fgkb0%3D&key=ozDCjbB0U95curW6%2Bbnxxw%3D%3D");
        doc.Save("test.pdf");
    }

    private static string? GetBlinkPath()
    {
        var loc = Path.GetDirectoryName(typeof(Program).Assembly.Location);
        Console.WriteLine($"Loc: {loc}");
        if (loc is null)
            return null;
        foreach (var dir in Directory.EnumerateDirectories(loc))
            if (Path.GetFileName(dir).ToLower().StartsWith("chromium"))
                return dir;
        return null;
    }
}