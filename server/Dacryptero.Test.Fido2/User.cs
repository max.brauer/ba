using System;
using System.Collections.Concurrent;
using Fido2NetLib;
using Fido2NetLib.Objects;

namespace Dacryptero.Test.Fido2
{
    public class User
    {
        public Guid Id { get; }

        public byte[] Challenge { get; set; } = Array.Empty<byte>();

        public PublicKeyCredentialDescriptor? Descriptor { get; set; }

        public byte[]? PublicKey { get; set; }

        public byte[]? UserHandle { get; set; }

        public uint Counter { get; set; }

        private User()
        {
            Id = Guid.NewGuid();
        }

        static ConcurrentDictionary<Guid, User> users = new ConcurrentDictionary<Guid, User>();

        public static User Create()
        {
            var user = new User();
            users.AddOrUpdate(user.Id, _ => user, (_, _) => user);
            return user;
        }

        public static User? Get(Guid id)
        {
            return users.TryGetValue(id, out User? user) ? user : null;
        }
    }
}