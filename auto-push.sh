#!/bin/bash

if [[ ! -d .git ]]; then
    echo "no git repo" >> /dev/fd/2
    exit 1
fi

if ! which inotifywait > /dev/null; then
    echo "Please install the tool inotifywait"
    exit 3
fi

function push_success () {
    echo "$(basename "$(pwd)") pushed"
    if which dunstify > /dev/null; then
        dunstify "$(basename "$(pwd)") pushed" -u normal -h string:x-dunst-stack-tag:git-push
    fi
}

function push_fail () {
    echo "$(basename "$(pwd)") push error"
    if which dunstify > /dev/null; then
        dunstify "$(basename "$(pwd)") push error. view logs for details" -u critical -h string:x-dunst-stack-tag:git-push
    fi
}

echo "Wait for commit"
while inotifywait -qr --event close_write,move,delete --format '%w%f' .git/refs/heads; do
    sleep 0.5
    git push && push_success || push_fail
done
